android {
    compileSdkVersion 29
    buildToolsVersion "29.0.2"

    //...............
    
    
    //QR
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
}

//------------------------------------------------------------------------------

Add Dependency
dependencies {

    // Google zxing (QR)
    implementation 'com.google.zxing:core:3.3.3'
    // Google Vision
    implementation 'com.google.android.gms:play-services-vision:19.0.0'
    // QR Code
    implementation('com.journeyapps:zxing-android-embedded:4.1.0') { transitive = false }
    
}