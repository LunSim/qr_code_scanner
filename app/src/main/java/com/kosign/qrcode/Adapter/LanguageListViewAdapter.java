package com.kosign.qrcode.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kosign.qrcode.LanguageActivity;
import com.kosign.qrcode.LoginActivity;
import com.kosign.qrcode.R;
import com.kosign.qrcode.model.Constants;
import com.kosign.qrcode.model.LanguageDlgAlert;
import com.kosign.qrcode.model.LanguageHelper;
import com.kosign.qrcode.model.LanguageItemObject;
import com.kosign.qrcode.model.LocaleHelper;
import com.kosign.qrcode.model.PreferenceDelegator;

import java.util.List;

import static java.security.AccessController.getContext;

public class LanguageListViewAdapter extends BaseAdapter {
    private Context mContext;
    private List<LanguageItemObject> mListLanguages;

    public LanguageListViewAdapter(Context context,List<LanguageItemObject> list){
        this.mContext = context;
        this.mListLanguages = list;
    }

    @Override
    public int getCount() {
        return mListLanguages.size();
    }

    @Override
    public Object getItem(int position) {
        return mListLanguages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            convertView = View.inflate(mContext, R.layout.activity_language_flag_item,null);
        }
        LanguageItemObject itemObject = mListLanguages.get(position);

        TextView tvLang =  convertView.findViewById(R.id.tv_lang);
        tvLang.setText(itemObject.getLocal_key());
        tvLang.setCompoundDrawablesWithIntrinsicBounds(LanguageHelper.getImageFromDrawable(mContext,itemObject.getFlag_code()),0,0,0);
        LinearLayout linearLayout = convertView.findViewById(R.id.ll_lang_item);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = LocaleHelper.setLocale(mContext, mListLanguages.get(position).getIso_code());
                Log.d(">>>", "onClick: "+mListLanguages.get(position).getIso_code());
                LanguageDlgAlert.DlgAlertOkCancel(mContext, context.getString(R.string.DlG_ALERT_TITER),
                        context.getString(R.string.LANGUAGE_MESSAGE), context.getString(R.string.DLG_ALERT_NO),
                        context.getString(R.string.DLG_ALERT_YES), new LanguageDlgAlert.OnDialogListener() {
                            @Override
                            public void onClickDlgButton(int dialogIndex, LanguageDlgAlert.DIALOG_BTN buttonType) {
                                if (dialogIndex == 1) {
                                    LocaleHelper.setLocale(mContext, itemObject.getIso_code());
                                    PreferenceDelegator pre = PreferenceDelegator.getInstance(mContext);
                                    pre.put(Constants.LoginInfo.NATION_CD, itemObject.getNation_cd());

                                    Intent intent = new Intent(mContext, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                            Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                            Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                            Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                                    (mContext.getApplicationContext()).startActivity(intent);

                                }
                            }
                        });
                notifyDataSetChanged();
            }
        });
        return convertView;
    }
}
