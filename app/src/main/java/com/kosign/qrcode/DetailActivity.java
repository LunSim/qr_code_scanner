package com.kosign.qrcode;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kosign.qrcode.model.Constants;
import com.kosign.qrcode.model.PreferenceDelegator;

import java.net.URISyntaxException;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {
    private Button qrCode, btn_logout;
    private TextView view_qr;
    private String data;
    String userNM;
    String userPW;
    String nationCD;
    PreferenceDelegator pre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initView(savedInstanceState);
        checkIntentData();

        if (data.startsWith("http")|| data.startsWith("https")){
            qrCode.setVisibility(View.VISIBLE);
            view_qr.setVisibility(View.GONE);
            qrCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    onStartIntent(intent);
                }
            });
            qrCode.setText(data);
        }else {
            qrCode.setVisibility(View.GONE);
            view_qr.setVisibility(View.VISIBLE);
            view_qr.setText(data);
        }

    }

    public void initView(Bundle savedInstanceState){
        qrCode = findViewById(R.id.qr_code);
        view_qr = findViewById(R.id.view_qr);
        btn_logout = findViewById(R.id.btn_logout);

        btn_logout.setOnClickListener(this);
    }

    /**
     * Intent Extra 데이터 검사
     */
    protected void checkIntentData () {
        try {
            Intent intent = getIntent();
            if (intent == null)
                return;

            // Extra 데이터 전달
            data  = intent.getStringExtra(IntentConst.Extras.EXTRA_WEB_CALLBACK_DATA);
            pre = PreferenceDelegator.getInstance(this);
            //userNM = getIntent().getStringExtra(Constants.LoginInfo.USER_ID);
            //userPW = getIntent().getStringExtra(Constants.LoginInfo.PASSWORD);
            //nationCD = pre.get(Constants.LoginInfo.NATION_CD);

            onStartIntent(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onStartIntent(Intent intent){
        if (data.startsWith("http")|| data.startsWith("https")){
            try {
                intent = Intent.parseUri(data,Intent.URI_INTENT_SCHEME);
                startActivity(intent);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btn_logout:
               if (pre.get(Constants.LoginInfo.USER_ID).equals("sim") && pre.get(Constants.LoginInfo.PASSWORD).equals("sim1234!")){
                   intent = new Intent(this,LanguageActivity.class);
                   pre.remove(Constants.LoginInfo.NATION_CD);
                   startActivity(intent);
               }
        }
    }
}
