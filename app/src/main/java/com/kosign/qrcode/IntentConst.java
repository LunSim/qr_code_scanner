package com.kosign.qrcode;

import static android.app.Activity.RESULT_OK;

public class IntentConst {
    class Extras{
        public final static String EXTRA_WEB_CALLBACK_DATA = "EXTRA_WEB_CALLBACK_DATA";
    }
    class ActResultCode{
        public static final int RES_ACT_OK              = RESULT_OK;
    }
    class PermissionRequestCode{
        /** 카메라 접근권한 요청 코드 */
        public static final int REQ_PERMISSIONS_CAMERA              = 0x1001;
    }
}
