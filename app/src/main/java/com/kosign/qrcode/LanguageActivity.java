package com.kosign.qrcode;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.kosign.qrcode.Adapter.LanguageListViewAdapter;
import com.kosign.qrcode.model.LanguageHelper;
import com.kosign.qrcode.model.LanguageItemObject;
import com.kosign.qrcode.model.LocaleHelper;

import java.util.ArrayList;

public class LanguageActivity extends AppCompatActivity {
    private ArrayList<LanguageItemObject> mLanguageLists;
    LanguageListViewAdapter mAdapter;
    private int  SELECTED_POSITION = -1;
    String prev_lang;
    TextView tv_lang_description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        mLanguageLists = LanguageHelper.getLanguageItems(this);
        Log.d("sim", "onCreate: "+mLanguageLists.size());
        tv_lang_description = findViewById(R.id.tv_language_title);
        tv_lang_description.setText(Html.fromHtml(getString(R.string.language_01)));
        //initFlag();
        mAdapter = new LanguageListViewAdapter(this,mLanguageLists);
        ListView listView = findViewById(R.id.lv_language);
        listView.setAdapter(mAdapter);
    }

    private void initFlag() {
        prev_lang = LocaleHelper.getLanguage(this);
        for (int i = 0; i < mLanguageLists.size(); i++){
            if (prev_lang != null && prev_lang.equals(mLanguageLists.get(i).getIso_code())){
                SELECTED_POSITION = i;
                Log.d("sim", "initFlag: "+prev_lang +", ISO_CODE:"+mLanguageLists.get(i).getIso_code());
            }
        }
    }
}
