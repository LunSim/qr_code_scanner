package com.kosign.qrcode;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.kosign.qrcode.model.ComLoading;
import com.kosign.qrcode.model.Constants;
import com.kosign.qrcode.model.PreferenceDelegator;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etID,etPWD;
    private ImageView iv_clear_id;
    private CheckBox switch_visibility;
    private Button btn_login;
    private ComLoading mLoading;
    private PreferenceDelegator pre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Log.d(">>>", "getNation_cd: "+ PreferenceDelegator.getInstance(this).get(Constants.LoginInfo.NATION_CD));
        mLoading    = new ComLoading(this);
        pre = PreferenceDelegator.getInstance(this);
        initView(savedInstanceState);
    }

    public void initView(Bundle savedInstanceState){
        etID = findViewById(R.id.etID);
        etPWD = findViewById(R.id.etPWD);
        iv_clear_id = findViewById(R.id.iv_clear_id);
        switch_visibility = findViewById(R.id.switch_visibility);
        btn_login = findViewById(R.id.btn_login);

        /*
         * set button listener
         */
        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btn_login:
                /*
                 * login click
                 */
                if (TextUtils.isEmpty(etID.getText().toString()) || TextUtils.isEmpty(etPWD.getText().toString())) {
                    showEmpty(getResources().getString(R.string.A005_9));
                } else {
                    callLogin();
                }
        }
    }

    private void callLogin() {
        if (!TextUtils.isEmpty(etID.getText().toString()) || !TextUtils.isEmpty(etPWD.getText().toString())){
            //mLoading.showProgressDialog();
            if (etID.getText().toString().equals("sim") && etPWD.getText().toString().equals("sim1234!")){
                Intent intent = new Intent(this,MainActivity.class);
                pre.put(Constants.LoginInfo.USER_ID,etID.getText().toString());
                pre.put(Constants.LoginInfo.PASSWORD,etPWD.getText().toString());
                /*intent.putExtra(Constants.LoginInfo.USER_ID,etID.getText().toString());
                intent.putExtra(Constants.LoginInfo.PASSWORD,etPWD.getText().toString());
                intent.putExtra(Constants.LoginInfo.INTENT_NATION_CD,PreferenceDelegator.getInstance(this).get(Constants.LoginInfo.NATION_CD));*/
                startActivity(intent);
                //mLoading.dismissProgressDialog();
            }
        }
    }

    private void showEmpty(String err_msg) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(getResources().getString(R.string.alert_info));
        builder.setMessage(err_msg);
        builder.setPositiveButton(getResources().getString(R.string.A010_2P_3),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
        AlertDialog dialog = builder.show();
        TextView messageView = (TextView) dialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER);
    }
}
