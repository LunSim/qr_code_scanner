package com.kosign.qrcode;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.BarcodeView;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.Size;
import com.journeyapps.barcodescanner.ViewfinderView;
import com.kosign.qrcode.Utils.PermissionUtil;

public class ScanningActivity extends AppCompatActivity implements DecoratedBarcodeView.TorchListener, View.OnClickListener {
    /** QR 코드 인식 매니저 */
    private CodeCaptureManager mCapture;

    /** QR 코드 인식 카메라 뷰 */
    private DecoratedBarcodeView mBarcodeScannerView;
    /** QR 코드 인식 카메라 뷰 > QR 코드 위치 뷰 */
    private ViewfinderView mViewfinderView;

    /** Scan Frame Width, Height **/
    private int scanFrameWidth, scanFrameHeight;

    private class CodeCaptureManager extends CaptureManager {

        /**
         * 생성자
         * @param activity Activity
         * @param barcodeView QR 코드 인식 카메라 뷰
         */
        public CodeCaptureManager(Activity activity, DecoratedBarcodeView barcodeView) {
            super(activity, barcodeView);
        }

        // QR 코드 인식 처리를 위해 재정의
        /**
         * QR 코드 인식 결과 처리
         * @param rawResult 인식 결과
         */
        @Override
        protected void returnResult(BarcodeResult rawResult) {
//            super.returnResult(rawResult);
            // 이전 화면으로 QR 코드 인식 결과 및 QR 코드 인식 화면 호출 시 전달한 데이터 Callback
            String strQrData = rawResult.getText();
            Log.d(">>>", "returnResult: "+strQrData);
            /*Intent resultData = new Intent();
            resultData.putExtra(IntentConst.Extras.EXTRA_WEB_CALLBACK_DATA, strQrData);
            if (getIntent() != null && getIntent().getExtras() != null) {
                resultData.putExtras(getIntent().getExtras());
            }

            setResult(IntentConst.ActResultCode.RES_ACT_OK, resultData);
            // QR 코드 인식 화면 종료
            closeAndFinish();*/
            Intent intent = new Intent(ScanningActivity.this,DetailActivity.class);
            intent.putExtra(IntentConst.Extras.EXTRA_WEB_CALLBACK_DATA,strQrData);
             startActivity(intent);


        }
    }
    @Override
    public void onTorchOn() {

    }

    @Override
    public void onTorchOff() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanning);
        initView(savedInstanceState);
    }
    public void initView(Bundle savedInstanceState){
        findViewById(R.id.btn_close).setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);

        mBarcodeScannerView = findViewById(R.id.zxing_barcode_scanner);
        mBarcodeScannerView.setTorchListener(this);

        mViewfinderView = findViewById(R.id.zxing_viewfinder_view);
        mViewfinderView.setLaserVisibility(false);

        mCapture = new CodeCaptureManager(this, mBarcodeScannerView);
        mCapture.initializeFromIntent(getIntent(), savedInstanceState);
        mCapture.setShowMissingCameraPermissionDialog(false);

        RelativeLayout rlScanFrame = findViewById(R.id.rl_scan_frame);
        rlScanFrame.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                scanFrameWidth = rlScanFrame.getWidth();
                scanFrameHeight = rlScanFrame.getHeight();

                Log.d(">>>", "scanFrameWidth: "+scanFrameWidth);
                Log.d(">>>", "scanFrameWidth: "+scanFrameHeight);
                rlScanFrame.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                BarcodeView barcodeView = mBarcodeScannerView.findViewById(R.id.zxing_barcode_surface);
                barcodeView.setFramingRectSize(new Size(scanFrameWidth, scanFrameHeight));

                if (!checkCameraPermission()) {
                    startScan();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_close || view.getId() == R.id.btn_cancel) {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCapture.onResume();
        mCapture.decode();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCapture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCapture.onDestroy();
    }
    public void startScan(){
        mCapture.decode();
    }

    private boolean checkCameraPermission () {
        // 카메라 접근권한 검사
        return PermissionUtil.checkPermission(this,
                IntentConst.PermissionRequestCode.REQ_PERMISSIONS_CAMERA,
                Manifest.permission.CAMERA
        );
    }
}
