package com.kosign.qrcode.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TimeZone;

/**
 * 변환 관련 util
 * <br/><br/>
 * 문자열, 날짜, 금액, 계좌번호, 카드번호 등 변환하여 추출한다<br/>
 *
 * @author Webcash Smart
 * @since 2017. 7. 20.
 */
public class Convert {

    /** 일자구분자 **/
    private static final String DATE_DELIMITER 		= ".";

    /** 일자구분자 **/
    private static final String TIME_DELIMITER 		= ":";

    /** 구분자없는 일자포멧 **/
    private static final String FORMAT_YYYYMMDD 	= "yyyyMMdd";

    /** 일시포멧 **/
    private static final String FORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    /** 요일배열 **/
    private static String[] DAY_OF_WEEK = {null, "일", "월", "화", "수", "목", "금", "토"};

    /**
     * 소수점 관련 변환
     * <br/><br/>
     * 소수점 관련 변환<br/>
     *
     * @author Webcash Smart
     * @since 2017. 7. 20.
     */
    public static class strRate {

        /**
         * 소수점 문자열을 포맷에 맞게 변환 (0 추가)
         * <br/><br/>
         * - ex) .xxx -> 0.xxx<br/>
         *
         * @param value 변환하려는 소수값
         * @return 변환된 소수값
         */
        public static String paddingZero(String value) {
            String str = value;
            if(value.substring(0,1).equals(".")) {
                str = "0" + value;
            }

            return str;
        }
    }

    /**
     * 문자열 금액 처리
     *
     * @author Webcash Smart
     * @since 2017. 7. 20.
     */
    public static class strAmount{

        /**
         * 금액 숫자를 화폐단위로 변환
         * <br/><br/>
         * - 예) 1000 -> 1,000<br/>
         *
         * @param value 변환하려는 금액 숫자
         * @return 변환된 금액 문자열
         */
        public static String format(double value) {
            String str = Double.toString(value);

            return format(str);
        }

        /**
         * 문자열 금액을 넣어주면 ',' 넣어서 반환해준다.
         *
         * @param value 변환하려는 금액 문자열
         * @return ',' 가 추가된 변환된 금액 문자열
         */
        public static String format(String value) {
            String str;

            if(value == null) return "";
            if(value.trim().equals(""))
                return "0";

            NumberFormat money = NumberFormat.getInstance(Locale.US);
            str = money.format(Double.parseDouble(value));
            return str;
        }

        /**
         * 문자열 금액에서 ‘,’ 표시를 전부 제거
         *
         * @param value 변환하려는 금액 문자열
         * @return ',' 를 제거한 변환된 금액 문자열
         */
        public static String nonFormat(String value) {
            String str = value.trim().replaceAll("\\,", "");
            str = str.replaceAll("\\*", "");

            return str;
        }

        /**
         * 문자열금액에서 소수점 첫째자리를 반올임하고 ',' 넣어서 반환
         *
         * @param value 변환하려는 금액 문자열
         * @return 변환된 금액 문자열
         */
        public static String formatRoundHalfUp(String value) {
            if(value == null) return "";
            if(value.trim().equals(""))
                return value;

            double dblVal = Double.parseDouble(value);
            BigDecimal bigVal =  BigDecimal.valueOf(dblVal);

            return format(bigVal.setScale(0, BigDecimal.ROUND_HALF_UP)+"");
        }

        /**
         * 금액 콤마 (',') 표시 해주고 소수점은 그대로 유지하여 반환
         *
         * @param value 변환하려는 금액 문자열
         * @return 변환된 금액 문자열
         */
        public static String fractZero(String value) {
            if(value == null) return "";
            if(value.trim().equals(""))
                return value;
            String[] temp = value.split("[.]");
            String digit = format(temp[0]);
            return digit + ((temp[1] == null || "".equals(temp[1]))? "" : "." + temp[1]);
        }
    }

    /**
     * 일자 관련 변환
     *
     * @author Webcash Smart
     * @since 2017. 7. 20.
     */
    public static class ComDate {

        /**
         * 한국표준시간(YYYYMMDDHHMMSS) 반환
         *
         * @return 한국표준시간 문자열 (YYYYMMDDHHMMSS)
         */
        public static String tranKST() {
            String result = "";

            Date currentTime = new Date ( );
            SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat (FORMAT_YYYYMMDDHHMMSS, Locale.KOREA );
            mSimpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

            result = mSimpleDateFormat.format ( currentTime );

            return result;
        }

        /**
         * 현재 Date 반환
         * <br/><br/>
         * - 입력받은 포맷에 맞는 현재 Date 을 제공한다.<br/>
         *
         * @param delem Date 포맷
         * @return 입력받은 포맷에 맞는 현재 Date 문자열
         */
        public static String CurrDate(String delem){
            Date today;
            today = Calendar.getInstance().getTime();

            SimpleDateFormat format = new SimpleDateFormat(delem);
            String strDay = format.format(today);

            return strDay;
        }

        /**
         * 현재 Date 에서 Delem 만큼 더해진 날짜 반환
         * <br/><br/>
         * - 더해진 날짜 포맷 : yyyyMMdd <br/>
         *
         * @param delem 더해질 날짜 (일수)
         * @return 더해진 날짜 (일자포맷 : yyyyMMdd)
         */
        public static String NextDate(int delem) throws Exception{
            return getAfterDate(delem);
        }

        /**
         * 현재일자를 반환
         * <br/><br/>
         * - 현재일자 포맷 : yyyyMMdd <br/>
         *
         * @return 현재일자 (일자포맷 : yyyyMMdd)
         */
        public static String today(){
            return CurrDate(FORMAT_YYYYMMDD);
        }

        /**
         * 사용자 지정 포멧으로 현재일자 반환
         * <br><br>
         * - 사용자 지정 포맷이 "" 인 경우 "yyyymmdd" 형식으로 반환 <br>
         * @param formmater 사용자 지정 포맷
         * @return 사용자 지정 포맷으로 변환한 현재 일자
         */
        public static String today(String formmater) {
            Date today;
            String dateFormatter = formmater;
            today = Calendar.getInstance().getTime();

            if(dateFormatter.equals("")) dateFormatter = FORMAT_YYYYMMDD;
            SimpleDateFormat format = new SimpleDateFormat(dateFormatter);
            String strDay = format.format(today);

            return strDay;
        }

        /**
         * 내일 일자를 반환
         * <br/><br/>
         * - 내일 일자 포맷 : yyyyMMdd <br/>
         *
         * @return 내일일자 (일자포맷 : yyyyMMdd)
         * @throws Exception
         */
        public static String tomorrow() throws Exception{
            return NextDate(1);
        }

        /**
         * 현재일자를 반환
         * <br/><br/>
         * - 일자포맷 : yyyyMMddHHmmss<br/>
         *
         * @return 현재일자 (일자포맷 : yyyyMMddHHmmss)
         */
        public static String currDateTime(){
            return CurrDate(FORMAT_YYYYMMDDHHMMSS);
        }

        /**
         * 현재일자를 반환
         * <br/><br/>
         * - 일자포맷 : yyyy-MM-dd HH:mm:ss SSS <br/>
         *
         * @return 현재일자 (일자포맷 : yyyy-MM-dd HH:mm:ss SSS )
         */
        public static String currDateTimeMillisend() {
            return CurrDate("yyyy-MM-dd HH:mm:ss SSS");
        }

        /**
         * 현재일자를 반환
         * <br/><br/>
         * - 일자포맷 : 사용자 지정 <br/>
         *
         * @param format 일자포맷
         * @return 현재일자 (일자포맷 : 사용자 지정)
         */
        public static String formatCurrDate(String format){
            return CurrDate(format);
        }

        /**
         * 현재일자를 반환
         * <br/><br/>
         * - 일자포맷 : YYYY.MM.DD <br/>
         *
         * @return 현재일자 (일자포맷 : YYYY.MM.DD)
         */
        public static String todayDelimiter(){
            return formatDelimiter(today());
        }

        /**
         * 내일일자를 반환
         * <br/><br/>
         * - 일자포맷 : YYYY.MM.DD <br/>
         *
         * @return 현재일자 (일자포맷 : YYYY.MM.DD)
         */
        public static String tomorrowDelimiter() throws Exception{
            return formatDelimiter(tomorrow());
        }


        /**
         * 해당 년월에 마지막 일자 반환
         * <br><br>
         * - 일자포맷 : YYYYMMDD <br>
         * - Convert.ComDate.lastDay("2018", "1") => 2018131 <br>
         * @param year 년도
         * @param month 월
         * @return 해당 년월에 마지막 일자
         */
        public static String lastDay(String year, String month){
            int day = lastDay(Integer.valueOf(year), Integer.valueOf(month));
            return year + month + pubCharL(String.valueOf(day), 2, "0");
        }

        /**
         * 해당월의 마지막 일을 반환
         * <br/><br/>
         * - Convert.ComDate.lastDay(2018, 1) => 31 <br>
         * @param year 년도
         * @param month 월
         * @return 해당 년월에 마지막 일
         */
        public static int lastDay(int year, int month){
            Calendar calender = Calendar.getInstance();
            calender.set(year, month-1, 1);
            int lastDay = calender.getActualMaximum(Calendar.DAY_OF_MONTH);
            return lastDay;
        }

        /**
         * 해당월의 마지막 일를 반환
         * <br/><br/>
         * - Convert.ComDate.lastDay("201801") => 31 <br/>
         *
         * @param yyyymm 년월
         * @return 해당 년월에 마지막 일
         */
        public static String lastDay(String yyyymm){
            int year = Integer.valueOf(yyyymm.substring(0, 4));
            int month = Integer.valueOf(yyyymm.substring(4, 6));
            Calendar calender = Calendar.getInstance();
            calender.set(year, month-1, 1);
            int lastDay = calender.getActualMaximum(Calendar.DAY_OF_MONTH);
            return String.valueOf(lastDay);
        }

        /**
         * 구분자가 없는 일자의 요일을 반환 (YYYYMMDD)
         * <br/><br/>
         * - YYYYMMDD 일자 포맷으로 전달 필요 <br>
         * - Convert.ComDate.dayOfWeek("20180120") => 토  (토요일)<br>
         * @param value 구분자가 없는 일자(YYYYMMDD)
         * @return 요일 문자열
         */
        public static String dayOfWeek(String value) {
            int year = Integer.parseInt(value.substring(0, 4));
            int month = Integer.parseInt(value.substring(4, 6));
            int day = Integer.parseInt(value.substring(6, 8));

            return dayOfWeek(year, month, day);
        }

        /**
         * 입력받은 년월일의 요일 반환
         * <br/><br/>
         * - Convert.ComDate.dayOfWeek(2018, 1,20) => 토 (토요일)<br>
         * @param year 년
         * @param month 월
         * @param day 일
         * @return 요일 문자열
         */
        public static String dayOfWeek(int year, int month, int day) {
            Calendar cal = Calendar.getInstance();
            cal.set(year, month - 1, day);
            int dayofweek = cal.get(Calendar.DAY_OF_WEEK);

            return DAY_OF_WEEK[dayofweek];
        }

        /**
         * 구분자가 없는 일자의 요일을 반환 (Calendar No)
         * <br/><br/>
         * - YYYYMMDD 일자 포맷으로 전달 필요 <br>
         * - Convert.ComDate.dayOfWeekNo("20180120") :: 7 (토요일) <br>
         *
         * @param value 구분자가 없는 일자(YYYYMMDD)
         * @return 요일 숫자 (Calendar No , 1 : 일요일 ~ 7 : 토요일)
         */
        public static int dayOfWeekNo(String value) {
            int year = Integer.parseInt(value.substring(0, 4));
            int month = Integer.parseInt(value.substring(4, 6));
            int day = Integer.parseInt(value.substring(6, 8));

            return dayOfWeekNo(year, month, day);
        }

        /**
         * 입력받은 년월일의 요일 반환 (Calendar No)
         * <br/><br/>
         * - Convert.ComDate.dayOfWeekNo(2018, 1,21) :: 1 (일요일)
         * @param year 년
         * @param month 월
         * @param day 일
         * @return 요일 숫자 (Calendar No , 1 : 일요일 ~ 7 : 토요일)
         */
        public static int dayOfWeekNo(int year, int month, int day) {
            Calendar cal = Calendar.getInstance();
            cal.set(year, month - 1, day);
            int dayofweek = cal.get(Calendar.DAY_OF_WEEK);

            return dayofweek;
        }

        /**
         * 구분자가 없는 일자의 괄호를 포함한 요일을 제공
         * <br/><br/>
         * - YYYYMMDD 일자 포맷으로 전달 필요 <br>
         * - Convert.ComDate.dayOfWeekWithBracket("20180120") => (토) <br>
         * @param value 구분자가 없는 일자(YYYYMMDD)
         * @return 괄호를 포함한 요일 문자열
         */
        public static String dayOfWeekWithBracket(String value) {
            return "(" + dayOfWeek(value) + ")";
        }

        /**
         * 현재시간과 입력시간의 5시간 이상 차이 여부 반환
         *
         * @param updateTime 입력시간 (time value : long)
         * @return 현재시간과 입력시간의 5시간 이상 차이 여부 (true : 5시간 이상 차이 / false : 5시간 미만 차이)
         */
        public static boolean is5hoursDiff(long updateTime){

            long diff = getTimeDiff(updateTime);
            if(diff >= 5) {
                return true;
            }
            return false;
        }

        /**
         * 입력된 시간과 현재 시간 차이 계산
         *
         * @param updateTime 입력시간 (time value : long)
         * @return 입력된 시간과 현재 시간 차이 (time value : long)
         */
        public static long getTimeDiff(long updateTime) {

            //현재의 시간 설정
            long currTime = getCurrentTime();
            long diff= currTime - updateTime;

            //시간 으로 변환
            long hours = (diff/60000) / 60;

            return hours;
        }

        /**
         * 현재 시간 반환
         *
         * @return 현재시간 (time value : long)
         */
        public static long getCurrentTime() {

            //현재의 시간 설정
            Calendar cal=Calendar.getInstance();
            Date endDate=cal.getTime();
            long currTime = endDate.getTime();

            return currTime;
        }

        /**
         * 현재 시간,분 반환
         * <br><br>
         * - hour + min 형식 <br>
         * @return 현재 시간,분
         */
        public static String getCurrHourMn() {
            String str = currDateTime().trim();

            String hour = str.substring(8, 10);
            String min = str.substring(10, 12);

            return hour + "" + min ;
        }

        /**
         * 현재일로부터 #일후의 날짜 자동 계산해서 리턴
         * <br/><br/>
         * - getAfterDate(int) <- 알고자 하는 몇일후의 날짜 입력<br/>
         * - 반환일자포맷 : YYYYMMDD<br>
         * @param day #일
         * @throws Exception
         * @return #일 후의 날짜
         */
        public static String getAfterDate(int day) throws Exception  {
            int y, m, d ;
            y = Integer.parseInt(today().substring(0, 4));
            m = Integer.parseInt(today().substring(4, 6));
            d = Integer.parseInt(today().substring(6, 8));

            return getAfterDate2(y, m, d, day);
        }

        /**
         * 특정일로부터 #일후의 날짜 자동 계산해서 리턴
         * <br/><br/>
         * - 반환일자포맷 : YYYYMMDD<br>
         * @param YYYYMMDD 특정일자
         * @param day #일
         * @throws Exception
         * @return #일 후의 날짜
         */
        public static String getAfterDate(String YYYYMMDD, int day) throws Exception {
            int y, m, d ;
            y = Integer.parseInt(YYYYMMDD.substring(0, 4));
            m = Integer.parseInt(YYYYMMDD.substring(4, 6));
            d = Integer.parseInt(YYYYMMDD.substring(6, 8));

            return getAfterDate2(y, m, d, day);
        }

        /**
         * 특정일로부터 #일후의 날짜 자동 계산해서 리턴
         * <br/><br/>
         * - 반환일자포맷 : YYYYMMDD<br>
         * @param YYYY 특정일자(년)
         * @param MM 특정일자(월)
         * @param DD 특정일자(일)
         * @param day #일
         * @throws Exception
         * @return #일 후의 날짜
         */
        public static String getAfterDate2(int YYYY, int MM, int DD, int day) throws Exception {
            Calendar temp=Calendar.getInstance();
            int y, m, d ;
            y = YYYY;
            m = MM - 1;
            d = DD ;
            temp.set(y, m, d);
            temp.add(Calendar.DAY_OF_MONTH, day);
            int nYear  = temp.get(Calendar.YEAR);
            int nMonth = temp.get(Calendar.MONTH) + 1;
            int nDay   = temp.get(Calendar.DAY_OF_MONTH);
            StringBuffer sbDate=new StringBuffer();
            sbDate.append(nYear);
            if (nMonth < 10)sbDate.append("0");
            sbDate.append(nMonth);
            if (nDay < 10)sbDate.append("0");
            sbDate.append(nDay);
            return sbDate.toString();
        }

        /**
         * 현재일로 부터 _Month 숫자후의 일자를 반환
         * <br/><br/>
         * - 반환형식 YYYYMMDD<br/>
         *
         * @param month #달
         * @return month 숫자후의 일자
         */
        public static String getAfterMonth(int month) {
            Calendar temp=Calendar.getInstance();

            temp.add(Calendar.MONTH, month);

            int nYear  = temp.get(Calendar.YEAR);
            int nMonth = temp.get(Calendar.MONTH) + 1;
            int nDay   = temp.get(Calendar.DAY_OF_MONTH);
            StringBuffer sbDate=new StringBuffer();
            sbDate.append(nYear);
            if (nMonth <10)sbDate.append("0");
            sbDate.append(nMonth);
            if (nDay <10)sbDate.append("0");
            sbDate.append(nDay);
            return sbDate.toString();
        }

        /**
         * 내일로 부터 _Month 숫자후의 일자를 반환한다.
         * <br/><br/>
         * - 반환일자포맷 : YYYYMMDD<br>
         *
         * @param month #달
         * @return month 숫자후의 일자
         */
        public static String getAfterMonth2(int month) {
            Calendar temp=Calendar.getInstance();

            temp.add(Calendar.MONTH, month);

            int nYear  = temp.get(Calendar.YEAR);
            int nMonth = temp.get(Calendar.MONTH) + 1;
            int nDay   = temp.get(Calendar.DAY_OF_MONTH)+1;
            StringBuffer sbDate=new StringBuffer();
            sbDate.append(nYear);
            if (nMonth <10)sbDate.append("0");
            sbDate.append(nMonth);
            if (nDay <10)sbDate.append("0");
            sbDate.append(nDay);
            return sbDate.toString();
        }

        /**
         * 내일로 부터 _Month 숫자후의 일자를 반환한다.
         * <br/><br/>
         * - 반환일자포맷 : YYYYMMDD<br>
         *
         * @param month #달
         * @return month 숫자후의 일자
         */
        public static String getAfterMonthAndInitializeDay(int month) {
            Calendar temp=Calendar.getInstance();

            temp.add(Calendar.MONTH, month);

            int nYear  = temp.get(Calendar.YEAR);
            int nMonth = temp.get(Calendar.MONTH) + 1;
            StringBuffer sbDate=new StringBuffer();
            sbDate.append(nYear);
            if (nMonth <10)sbDate.append("0");
            sbDate.append(nMonth);
            sbDate.append("01");
            return sbDate.toString();
        }

        /**
         * 해당일자의 요일을 반환
         * <br/><br/>
         * - 일자포맷 : YYYYMMDD<br>
         *
         * @param aDate (YYYYMMDD)
         * @return 요일 (월,화,수,목,금,토,일)
         */
        public static String getDayOfWeek(String aDate) {
            String strDate = aDate.replace(".", "");
            String strReturn = "";
            int Year = Integer.parseInt(strDate.substring(0, 4));
            int Month = Integer.parseInt(strDate.substring(4, 6));
            int Date = Integer.parseInt(strDate.substring(6, 8));
            Calendar cal = Calendar.getInstance();
            cal.set(Year, Month - 1, Date);
            switch (cal.get(Calendar.DAY_OF_WEEK)) {
                case  Calendar.SUNDAY :
                    strReturn = "일";
                    break;
                case Calendar.MONDAY :
                    strReturn = "월";
                    break;
                case Calendar.TUESDAY :
                    strReturn = "화";
                    break;
                case Calendar.WEDNESDAY :
                    strReturn = "수";
                    break;
                case Calendar.THURSDAY :
                    strReturn = "목";
                    break;
                case Calendar.FRIDAY :
                    strReturn = "금";
                    break;
                case Calendar.SATURDAY :
                    strReturn = "토";
                    break;
            }
            return strReturn;
        }

        /**
         * 전달한 구분자를 사용한 일자 데이터 (value) 를 yyyymmdd 형태의 형식으로 변환
         * <br/><br/>
         * - changeDateType("2010-6-1", "-") -> '20100601' return<br/>
         *
         * @param value 일자
         * @param delim 구분자
         * @return changeDateType("2010-6-1", "-") -> '20100601'
         */
        public static String changeDateType(String value, String delim) {
            String strDate = "";
            if(value == null || value.equals("")) return strDate;

            StringTokenizer stkBegin = new StringTokenizer(value, delim);
            String str = null;
            while(stkBegin.hasMoreElements()){
                str = stkBegin.nextToken();
                if(str.length() < 2)	str = Convert.pubCharL(str, 2, "0");
                strDate += str;
            }
            return strDate;
        }

        /**
         * 일자 구분자 도트(.) Format 적용하여 일자 반환
         * <br/><br/>
         * - 파라미터 일자 포맷 : YYYYMMDD <br/>
         * - 반환 일자 포맷 : YYYY.MM.DD <br>
         * @param value 구분자가 없는 일자(YYYYMMDD)
         * @return 구분자가 있는 일자(YYYY.MM.DD)
         */
        public static String formatDelimiter(String value) {
            return formatDotDelimiter(value, DATE_DELIMITER);
        }

        /**
         * 사용자 지정 구분자 Format 적용 일자 반환
         * <br/><br/>
         * - 파라미터 일자 포맷 : YYYYMMDD <br/>
         *
         * @param value 구분자가 없는 일자(YYYYMMDD)
         * @param del 사용자 지정 구분자
         * @return 사용자가 지정한 구분자가 있는 일자
         */
        public static String formatDotDelimiter(String value, String del) {
            if (value.length() != 8) return value;
            String str;
            str = value.substring(0, 4) + del +
                    value.substring(4, 6) + del +
                    value.substring(6, 8);
            return str;
        }

        /**
         * yyyyMMddHHmmss -> yyyy.MM.dd HH:mm:ss 변환
         *
         * @param value yyyyMMddHHmmss
         * @return yyyy.MM.dd HH:mm:ss
         */
        public static String formatDotDelimiter(String value) {
            if (value.length() != 14) return value;

            String str;
            str = value.substring(0, 4) + DATE_DELIMITER +
                    value.substring(4, 6) + DATE_DELIMITER +
                    value.substring(6, 8) + " " +
                    value.substring(8,10) + ":" +
                    value.substring(10,12) + ":" +
                    value.substring(12,14);

            return str;
        }

        /**
         * 도트(.) 구분자가 있는 일자를 구분자가 없는 일자로 반환
         * <br/><br/>
         * - 파라미터 일자 포맷 : YYYY.MM.DD <br/>
         * - 반환 일자 포맷 : YYYYMMDD <br>
         * @param value 도트(.)구분자가 있는 일자(YYYY.MM.DD)
         * @return 구분자가 없는 일자(YYYYMMDD)
         */
        public static String formatNoneDelimiter(String value) {
            String str;

            str = value.substring(0,4) +
                    value.substring(5,7) +
                    value.substring(8,10);

            return str;
        }

        // TODO :: kkb :: StrAmount 클래스로 이동 필요 ...
        /**
         * 콤마(,) 구분자가 있는 금액을 구분자가 없는 금액으로 제공
         * <br/><br/>
         *
         * @param value 콤마(,)구분자가 있는 일자(123,456,789)
         * @return 구분자가 없는 일자(123,456,789)
         */
        public static String formatNoneComma(String value) {
            return value.replaceAll(",", "");
        }



        /**
         * 구분자가 없는 년월일 반환(한 자리 달 또는 날짜 앞에 0을 붙임)
         * <br/><br/>
         * - 반환 일자 포맷 : YYYYMMDD <br>
         *
         * @param year 년
         * @param month 월
         * @param day 일
         * @return 구분자가 없는 년월일 (YYYYMMDD)
         */
        public static String formatNoneDelimiter(int year, int month, int day) {
            return String.valueOf(year) +
                    Convert.pubCharL(String.valueOf(month + 1), 2, "0") +
                    Convert.pubCharL(String.valueOf(day), 2, "0");
        }

        /**
         * TextView 에 설정되어 있는 문자열을 '.' 구분자가 없는 문자열로 반환
         *
         * @param view Activity view
         * @param tv TextView
         * @param id TextView id
         * @return '.' 구분자가 없는 문자열로 반환
         */
        public static String formatNoneDelimiter(View view, TextView tv, int id ) {
            String str = ((TextView)view.findViewById(id)).getText().toString();
            return str.replaceAll(DATE_DELIMITER, "");
        }

        /**
         * 도트(.) 구분자가 있는 일자 YYYY.MM.DD -> 'YYYY년 MM월 DD일' 로 반환
         * <br/><br/>
         * - 파라미터 일자 포맷 : YYYY.MM.DD <br/>
         * - 반환 일자 포맷 : YYYY년 MM월 DD일 <br>
         *
         * @param value 일자 값
         * @return 'YYYY년 MM월 DD일'
         */
        public static String formatKorYYmmDD(String value) {
            String str = value.replace(DATE_DELIMITER, "").trim();
            if(str.length() != 8) return value;

            String year = str.substring(0, 4);
            String month = str.substring(4, 6);
            String day = str.substring(6, 8);
            month = String.valueOf(Integer.valueOf(month));
            day = String.valueOf(Integer.valueOf(day));

            return year + "년 " + month + "월 " + day + "일";
        }


        /**
         * 현재 일자 제공
         * <br/><br/>
         * - 반환 일자 포맷 : YYYY년 MM월 DD일 HH시 mm분 ss초<br>
         *
         * @return YYYY년 MM월 DD일 HH시 mm분 ss초
         */
        public static String formatKorCurrTime() {
            String str = currDateTime().trim();

            String year = str.substring(0, 4);
            String month = str.substring(4, 6);
            String day = str.substring(6, 8);
            String hour = str.substring(8, 10);
            String min = str.substring(10, 12);
            String sec = str.substring(12, 14);

            month = String.valueOf(Integer.valueOf(month));
            day = String.valueOf(Integer.valueOf(day));

            return year + "년 " + month + "월 " + day + "일 " + hour + "시 " + min + "분 " + sec + "초";
        }


        /**
         * 도트(.) 구분자가 있는 일자 (YYYY.MM.DD) 에서 월/일을 'MM월 DD일' 로 반환
         * <br/><br/>
         * - 파라미터 일자 포맷 : YYYY.MM.DD <br/>
         * - 반환 일자 포맷 : MM월 DD일 <br>
         *
         * @param value 일자 값 (YYYY.MM.DD)
         * @return MM월 DD일
         */
        public static String formatKormmDD(String value) {
            String str = value.replace(DATE_DELIMITER, "").trim();
            if(str.length() != 8) return value;

            String year = str.substring(0, 4);
            String month = str.substring(4, 6);
            String day = str.substring(6, 8);
            month = String.valueOf(Integer.valueOf(month));
            day = String.valueOf(Integer.valueOf(day));

            return  month + "월 " + day + "일";
        }

        /**
         * 입력된 Millisecond time을 년월일시분으로 변환
         * <br/><br/>
         * - 반환 일자 포맷 : YYYY년 MM월 DD일 HH시 mm분 ss초<br>
         * @param millisTime Millisecond time
         * @return YYYY년 MM월 DD일 HH시 mm분 ss초
         */
        public static String formatMillisTime(long millisTime) {
            Date date = new Date(millisTime);
            SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat (FORMAT_YYYYMMDDHHMMSS, Locale.KOREA );
            mSimpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

            String formatDate = mSimpleDateFormat.format ( date );

            String str = formatDate.trim();
            String year = str.substring(0, 4);
            String month = str.substring(4, 6);
            String day = str.substring(6, 8);
            String hour = strTime.convertTime(str.substring(8, 10));
            String min = str.substring(10, 12);

            month = String.valueOf(Integer.valueOf(month));
            day = String.valueOf(Integer.valueOf(day));

            return year + "년 " + month + "월 " + day + "일 " + hour + "시 " + min + "분 ";
        }

        // TODO :: kkb :: formatDelimiter () 와 동일... 다른 기능으로 사용하거나 삭제.. 필요..
        /**
         * 일자 구분자 도트(.) Format 적용하여 일자 반환
         * <br/><br/>
         * - 파라미터 일자 포맷 : YYYYMMDD <br/>
         * - 반환 일자 포맷 : YYYY.MM.DD <br>
         *
         * @param value 구분자가 없는 일자(YYYYMMDD)
         * @return 구분자가 있는 일자(YYYY.MM.DD)
         */
        public static String formatYYYYMMDD(String value) {
            String day = null;
            if ( value == null || value.length() < 8 ) {
                day = value;
            } else {
                StringBuffer sbr = new StringBuffer();
                sbr.append(value.substring(0,4));
                sbr.append(DATE_DELIMITER);
                sbr.append(value.substring(4,6));
                sbr.append(DATE_DELIMITER);
                sbr.append(value.substring(6,8));
                day = sbr.toString();
            }
            return day;
        }

        /**
         * 일자 구분자 도트(.) Format 적용하여 일자 반환
         * <br/><br/>
         * - 파라미터 일자 포맷 : YYYYMMDD <br/>
         * - 반환 일자 포맷 : YYYY.M.D <br>
         *
         * @param value 구분자가 없는 일자(YYYYMMDD)
         * @return 구분자가 있는 일자(YYYY.M.D)
         */
        public static String formatYYYYMD(String value) {
            String day = null;

            if ( value == null || value.length() < 8 ) {
                day = value;
            } else {
                String yyyy = value.substring(0,4);
                String mm = value.substring(4,6);
                String dd = value.substring(6,8);

                day = formatYYYYMD(yyyy, mm, dd);
            }
            return day;
        }

        /**
         * 일자 구분자 도트(.) Format 적용하여 일자 반환
         * <br/><br/>
         * - 파라미터 일자 포맷 : YYYYMMDD <br/>
         * - 반환 일자 포맷 : YY.MM.DD <br>
         *
         * @param value 구분자가 없는 일자(YYYYMMDD)
         * @return 구분자가 있는 일자(YY.MM.DD)
         */
        public static String formatYYMMDD(String value) {
            String day = null;

            if ( value == null || value.length() < 8 ) {
                day = value;
            } else {
                String yy = value.substring(2,4);
                String mm = value.substring(4,6);
                String dd = value.substring(6,8);

                day = yy + DATE_DELIMITER + mm + DATE_DELIMITER + dd;
            }
            return day;
        }

        /**
         * 전달한 년 (YYYY) / 월 (MM) / 일 (DD) 를 YYYY. M. D 형식으로 변환하여 반환
         * <br/><br/>
         * - 입력된 값이 2018, 01, 01일 경우 2018. 1. 1로 결과값 반환<br/>
         * - 입력된 값이 2018, 01, 20일 경우 2018. 1.20으로 결과값 반환 <br>
         * - 반환 일자 포맷 : YYYY.M.D <br>
         * @param YYYY 년 (YYYY)
         * @param MM 월 (MM)
         * @param DD 일 (DD)
         * @return 구분자가 있는 일자(YYYY.M.D)
         */
        public static String formatYYYYMD(String YYYY, String MM, String DD) {
            String day = null;
            String yyyy = YYYY;
            String mm = MM;
            String dd = DD;

            StringBuffer sbr = new StringBuffer();
            sbr.append(yyyy);
            sbr.append(DATE_DELIMITER);
            mm = MM.substring(0, 1);
            if(mm.equals("0")){
                sbr.append((MM.replace('0', ' ')).trim());
            } else {
                sbr.append(MM);
            }
            sbr.append(DATE_DELIMITER);
            dd = DD.substring(0, 1);
            if(dd.equals("0")){
                sbr.append((DD.replace('0', ' ')).trim());
            } else {
                sbr.append(DD);
            }
            day = sbr.toString();

            return day;
        }



        /**
         * 시간 관련 변환
         *
         * @author Webcash Smart
         * @since 2017. 7. 20.
         */
        public static class strTime {

            /**
             * 24시간 시간표시 문자열을 오전/오후시간으로 변환
             * <br/><br/>
             * - 사용예) converetTime("14") => 오후2시<br/>
             *
             * @param value 시간 문자열
             * @return 오전/오후 시간으로 변환된 문자열
             */
            public static String convertTime(String value) {
                int intTime = Integer.valueOf(value);
                String rsltTime = "";
                if(intTime <= 11) 		// 오전 0시 ~ 오전11시
                    rsltTime = "오전 " + String.format("%02d", intTime);
                else if(intTime == 12)	// 오후 12시
                    rsltTime = "오후 " + String.format("%02d", intTime);
                else if(intTime >= 13  || intTime <= 23) {	// 오후1시 ~ 오후11시
                    intTime  = intTime - 12;
                    rsltTime = "오후 " + String.format("%02d", intTime);
                }
                else if(intTime == 24 || intTime == 0)	// 오후 24시
                    rsltTime = "오전 00";
                return rsltTime;
            }

            /**
             * 시분초 또는 시분 문자열을 오전/오후 시간 문자열로 변환
             * <br/><br/>
             * - 사용예) convertKorTime("0335") => 오전 03:35 <br/>
             *
             * @param value hhmmss 또는 hhmm 문자열 (초단위를 전달하더라도 분단위 까지만 변환하여 반환)
             * @return 오전/오후 시간으로 변환된 문자열
             */
            public static String convertKorTime(String value) {
                String hour = "";
                if (value.length() >= 4) {
                    hour = convertTime(value.substring(0, 2));
                    return hour + ":" + value.substring(2, 4);
                } else {
                    return value;
                }
            }

            /**
             * 시분초 또는 시분 문자열을 오전/오후 시간 문자열로 변환
             * <br/><br/>
             * - 사용예) convertKorTimeHourMin("0335") => 오전 03시35분<br/>
             *
             * @param value hhmmss 또는 hhmm 문자열 (초단위를 전달하더라도 분단위 까지만 변환하여 반환)
             * @return 오전/오후 시간으로 변환된 문자열
             */
            public static String convertKorTimeHourMin(String value) {
                String hour = "";
                if (value.length() >= 4) {
                    hour = convertTime(value.substring(0, 2));
                    return hour + "시" + value.substring(2, 4)+ "분";
                } else {
                    return value;
                }
            }

            /**
             * 구분자가 없는 시간를 콜론(:) 구분자가 있는 시간으로 제공
             * <br/><br/>
             * - 사용예) formatDelimiter("0335") => 03:35<br/>
             *
             * @param value 구분자가 없는 시간 문자열 (HHMMSS 또는 HHMM)
             * @return 콜론(:)구분자가 있는 시간 문자열 (HH:MM:SS 또는 HH:MM)
             */
            public static String formatDelimiter(String value) {
                return formatTimeDelimiter(value, TIME_DELIMITER);
            }

            /**
             * 구분자가 없는 시간를 파라미터로 전달한 구분자를 사용하여 구분자가 있는 시간으로 제공
             * <br/><br/>
             * - 사용예) 구분자를 ":" 전달한 경우 : formatTimeDelimiter("0335") => 03:35<br/>
             *
             * @param value 구분자가 없는 시간 문자열 (HHMMSS 또는 HHMM)
             * @param del 구분자
             * @return 사용자 지정 구분자가 있는 시간 문자열
             */
            public static String formatTimeDelimiter(String value, String del) {
                String str = "";

                if(value.length() == 4){
                    str = value.substring(0, 2) + del +
                            value.substring(2, 4);
                }else if(value.length() == 6){
                    str = value.substring(0, 2) + del +
                            value.substring(2, 4) + del +
                            value.substring(4, 6);
                }
                return str;
            }
        }
    }

    /**
     * 카드정보 관련 변환
     *
     * @author Webcash Smart
     * @since 2017. 7. 20.
     */
    public static class Card {

        /**
         * 카드번호를 4자리마다 '-' 처리하여 반환
         * <br/><br/>
         * - 사용예) 1234567891011121 => 1234-5678-9101-1121 <br/>
         * @param value 카드번호 문자열
         * @return 변환된 카드번호 문자열
         */
        public static String format(String value) {
            String str = value;
            StringBuffer sbf = new StringBuffer();
            sbf.append(str.substring(0,4) + "-");
            sbf.append(str.substring(4,8) + "-");
            sbf.append(str.substring(8, 12) + "-");
            sbf.append(str.substring(12));

            return sbf.toString();
        }

        /**
         * 카드번호를 4자리마다 '-' 처리하여 반환
         * <br/><br/>
         * - 가운데 두 값은 * 처리<br/>
         * - 사용예) 1234567891011121 => 1234-****-****-1121 <br/>
         * @param value 카드번호 문자열
         * @return 변환된 카드번호 문자열
         */
        public static String formatToAsta(String value) {
            String str = value;
            StringBuffer sbf = new StringBuffer();
            sbf.append(str.substring(0,4) + "-");
            sbf.append("****" + "-");
            sbf.append("****" + "-");
            sbf.append(str.substring(12));

            return sbf.toString();
        }

        // TODO :: kkb :: Card.format() 과 동일, 통합 필요
        /**
         * 카드번호를 4자리마다 '-' 처리하여 반환
         * <br/><br/>
         * - 사용예) 1234567891011121 => 1234-5678-9101-1121 <br/>
         * - 15 자리 이상인 경우에만 사용가능 <br/>
         * @param cardNo 카드번호 문자열
         * @return 변환된 카드번호 문자열 (전달받은 카드번호 문자열이 15자리 미만이거나 null 인경우 "" 반환)
         */
        public static String formatCardNo(String cardNo) {
            String result = "";
            if ( cardNo != null && cardNo.length() >= 15 ) {
                StringBuffer sbr = new StringBuffer();
                sbr.append(cardNo.substring(0,4));
                sbr.append("-");
                sbr.append(cardNo.substring(4,8));
                sbr.append("-");
                sbr.append(cardNo.substring(8,12));
                sbr.append("-");
                sbr.append(cardNo.substring(12,cardNo.length()));
                result = sbr.toString();
            }
            return result;
        }

        // TODO :: kkb :: Card.formatToAsta() 과 동일, 통합 필요
        /**
         * 카드번호를 4자리마다 '-' 처리하여 반환
         * <br/><br/>
         * - 가운데 두 값은 * 처리<br/>
         * - 사용예) 1234567891011121 => 1234-****-****-1121 <br/>
         * - 15 자리 이상인 경우에만 사용가능 <br/>
         * @param cardNo 카드번호 문자열
         * @return 변환된 카드번호 문자열 (전달받은 카드번호 문자열이 15자리 미만이거나 null 인경우 "" 반환)
         */
        public static String formatCardNoToAstaWitHyphens(String cardNo) {
            String result = "";
            if ( cardNo != null && cardNo.length() >= 15 ) {
                StringBuffer sbr = new StringBuffer();
                sbr.append(cardNo.substring(0,4));
                sbr.append("-");
                sbr.append("****");
                sbr.append("-");
                sbr.append("****");
                sbr.append("-");
                sbr.append(cardNo.substring(12,cardNo.length()));
                result = sbr.toString();
            }
            return result;
        }

        /**
         * 4자리마다 '-' 처리된 카드번호 (XXXX-XXXX-XXXX-XXXX) 를 가운데 두 값은 * 처리하여 반환
         * <br/><br/>
         * - 사용예 ) 1234-5678-9101-1121 => 1234-****-****1121<br/>
         * @param cardNo 카드번호 문자열
         * @return 변환된 카드번호 문자열
         */
        public static String formatCardNoToAsta(String cardNo) {
            if(null == cardNo) return cardNo; // 9430030967341999
            String[] temp = cardNo.split("-");
            if(temp.length < 4) return cardNo;
            StringBuffer sbr = new StringBuffer();
            sbr.append(temp[0]);
            sbr.append("-");
            sbr.append("****");
            sbr.append("-");
            sbr.append("****");
            sbr.append("-");
            sbr.append(temp[3]);
            return sbr.toString();
        }
    }

    /**
     * 계좌번호 정보 관련 유틸리티
     * <br/><br/>
     * 계좌번호 정보 관련 클래스<br/>
     *
     * @author Webcash Smart
     * @since 2017. 7. 20.
     */
    public static class Account {

        /**
         * 우리은행 계좌번호를 '-' 처리하여 반환
         * <br/><br/>
         * - 14자리 또는 16자리만 가능 <br>
         * - 14자리 : 일반 계좌번호 (11112222333344 => 111-122223-33-344) <br>
         * - 16자리 : 외화계좌번호 (1111222233334444 => 111-122223-33-34444)<br>
         *
         * @param value 우리은행 계좌번호 문자열
         * @return '-' 처리된 우리은행 계좌번호 문자열
         */
        public static String format(String value) {
            if (value == null) return "";

            StringBuffer sbr = new StringBuffer();

            if(value.trim().length() == 14) {		// 일반계좌번호
                sbr.append(value.substring(0,3));
                sbr.append("-");
                sbr.append(value.substring(3,9));
                sbr.append("-");
                sbr.append(value.substring(9,11));
                sbr.append("-");
                sbr.append(value.substring(11,14));

                return sbr.toString();
            }else if(value.trim().length() == 16){	// 외화계좌번호
                sbr.append(value.substring(0,3));
                sbr.append("-");
                sbr.append(value.substring(3,9));
                sbr.append("-");
                sbr.append(value.substring(9,11));
                sbr.append("-");
                sbr.append(value.substring(11,16));
                return sbr.toString();
            }else {
                return value;
            }

        }

        /**
         * 우리은행 계좌번호를 '-' 처리하여 반환
         * <br/><br/>
         * - 마지막 자리 * 처리 <br>
         * - 14자리만 사용 가능 <br>
         * - 14자리 : 일반 계좌번호 (11112222333344 => 111-122223-33-344) <br>
         * @param value 우리은행 계좌번호 문자열
         * @return 변환된 우리은행 계좌번호 문자열
         */
        public static String formatToAsta(String value) {
            if (value == null) return "";

            StringBuffer sbr = new StringBuffer();

            sbr.append(value.substring(0,3));
            sbr.append("-");
            sbr.append(value.substring(3,9));
            sbr.append("-");
            sbr.append(value.substring(9,11));
            sbr.append("-");
            sbr.append("***");

            return sbr.toString();
        }

        /**
         * 계좌번호 마지막 3자리를 '***' 처리하여 반환
         *
         * @param value 계좌번호 문자열
         * @return 계좌번호 마지막 3자리를 '***' 처리한 계좌번호 문자열
         */
        public static String formatToLastAsta(String value) {
            if (value == null) return "";

            StringBuffer sbr = new StringBuffer();
            sbr.append(value.substring(0,value.length()-3));
            sbr.append("***");

            return sbr.toString();
        }

        /**
         * 계좌번호 '-' 구분자가 포함된 변환할 문자열의 특정 순번 '*' 처리하여 반환
         *
         * @param value 변환할 계좌번호 문자열 ( '-' 구분자 있음 )
         * @param position 변환할 영역
         *                 1) '-' 구분자로 나누었을때의 순번 0부터 시작 <br>
         *                 2) 111-122223-33-344 일 때 position = 2 이면 111-122223-**-344 <br>
         * @return 변환된 계좌번호 문자열
         */
        public static String formatToAsta(String value, int position) {
            if (value == null) return "";

            if( !value.contains("-") ) {

            }
            String[] values = value.split("-");
            StringBuffer sbr = new StringBuffer();

            for(int i=0;i<values.length; i++) {
                if( i == position ) {
                    sbr.append(String.format("%*"+values[i].length(), values[i]) + "-");
                } else
                    sbr.append(values[i] + "-");
            }

            // 막지막에 들어간 - 제거
            sbr.substring(0, sbr.length()-1);

            return sbr.toString();
        }

        /**
         * 계좌번호 문자열을 '-' 처리와 문자열 영역의 특정 순번 '*' 처리하여 반환 (일반/외환)
         * <br/><br/>
         * - 14자리 또는 16자리만 가능 <br>
         * - 일반계좌번호 (14자리) 11112222333344 일 경우 <br>
         * 1) 111 : position = 0 <br>
         * 2) 122223 : position = 1 <br>
         * 3) 33 : position = 2 <br>
         * 4) 344 : position = 3 <br>
         * - 외화계좌번호 (16자리) 1111222233334444 일 경우 <br>
         * 1) 111 : position = 0 <br>
         * 2) 122223 : position = 1 <br>
         * 3) 33 : position = 2 <br>
         * 4) 34444 : position = 3 <br>
         * @param value 변환할 계좌번호 문자열 ( - 구분자 있음 )
         * @param position 변환할 영역
         *                 1) '-' 구분자로 나누었을 때의 순번 0부터 시작 <br>
         *                 2) 11112222333344 일 때 position = 2 이면 '33' 이 ** 로 변환 <br>
         * @return 변환된 계좌번호 문자열
         */
        public static String formatToAstaAcc(String value, int position) {
            if (value == null) return "";

            StringBuffer sbr = new StringBuffer();

            if(value.trim().length() == 14) {		// 일반계좌번호
                sbr.append(position == 0? "***":value.substring(0,3));
                sbr.append("-");
                sbr.append(position == 1? "******":value.substring(3,9));
                sbr.append("-");
                sbr.append(position == 2? "**":value.substring(9,11));
                sbr.append("-");
                sbr.append(position == 3? "***":value.substring(11,14));

                return sbr.toString();
            }else if(value.trim().length() == 16){	// 외화계좌번호
                sbr.append(position == 0? "***":value.substring(0,3));
                sbr.append("-");
                sbr.append(position == 1? "******":value.substring(3,9));
                sbr.append("-");
                sbr.append(position == 2? "**":value.substring(9,11));
                sbr.append("-");
                sbr.append(position == 3? "*****":value.substring(11,16));
                return sbr.toString();
            }else {
                return value;
            }
        }

        /**
         * 대쉬(-) 구분자가 있는 계좌번호를 구분자가 없는 계좌번호로 반환
         *
         * @param value 대쉬(-)구분자가 있는 계좌번호(###-######-##-###)
         * @return 구분자가 없는 계좌번호(##############)
         */
        public static String nonFormatAcct(String value) {
            return value.trim().replaceAll("-", "");
        }
    }

    /**
     * 주민번호/사업자번호 포멧을 적용하여 반환
     * <br/><br/>
     * - 10자리인 경우 사업자번호 포멧 (###-##-#####) 리턴<br/>
     * - 13자리인 경우 주민번호 포멧 (######-#######) 리턴<br/>
     *
     * @param value 주민번호/사업자번호
     * @return 주민번호/사업자번호 포멧을 적용된 번호
     */
    public static String formatCorpNo(String value) {
        String number = null;
        if ( value != null && value.length() == 10 ) {
            StringBuffer sbr = new StringBuffer();
            sbr.append(value.substring(0,3));
            sbr.append("-");
            sbr.append(value.substring(3,5));
            sbr.append("-");
            sbr.append(value.substring(5,10));
            number = sbr.toString();
        } else if( value != null && value.length() == 13 ) {
            StringBuffer sbr = new StringBuffer();
            sbr.append(value.substring(0,6));
            sbr.append("-");
            sbr.append(value.substring(6));
            number = sbr.toString();
        } else {
            number = value;
        }
        return number;
    }

    /**
     * 메세지의 '&' 값을 vargs의 값으로 replace
     *
     * @param msg '&' 이 포함된 message
     * @param vargs message 값의 '&' 에 들어갈 값
     * @return message값의 '&' 을 vargs 의 값들로 변환한 데이터
     */
    public static String conStr(String msg, String...vargs) {
        String tmp = msg;
        for(String var : vargs){
            tmp = tmp.replaceFirst("&", var);
        }
        return tmp;
    }

    /**
     * null 값 공백처리
     *
     * @param str string 값
     * @return 공백 제거된 String || 빈값이면 "" 리턴턴	 */
    public static String null2void(String str) {
        if (str == null || "null".equals(str)) {
            return "";
        } else {

        }
        return str.trim();
    }

    /**
     * null 값 space " " 처리
     *
     * @param str string 값
     * @return 공백 제거된 String || 빈값이면 " " 리턴
     */
    public static String null2blank(String str) {
        if (str == null || "null".equals(str)) {
            return " ";
        }
        return str.trim();
    }

    /**
     * 문자열 앞으로 특정문자열 채우기
     *
     * @param str 문자열
     * @param len 문자열 최대길이
     * @param chr 문자열 앞으로 채울 특정문자
     * @return 특정문자열로 채워진 문자열
     */
    public static String pubCharL(String str, int len, String chr)	{
        int strLen = str.length();
        if(strLen < len)	{
            String rtnStr = str;
            for(int i= 0; i< len-strLen ; i++){
                rtnStr = chr + rtnStr;
            }
            return rtnStr;
        } else {
            return str;
        }
    }

    /**
     * 숫자 앞으로 특정문자열 채우기
     * @param n 숫자
     * @param len 숫자 최대길이
     * @param chr 숫자 앞으로 채울 특정문자
     * @return 특정문자열로 채워진 숫자 문자열
     */
    public static String pubCharL(int n, int len, String chr)	{
        return pubCharL(String.valueOf(n), len, chr);
    }

    /**
     * 문자열 뒤로 특정문자열 채우기
     * @param str 문자열
     * @param len 문자열 최대길이
     * @param chr 문자열 뒤로 채울 특정문자
     * @return 특정문자열로 채워진 문자열
     */
    public static String pubCharR(String str, int len, String chr)	{
        int strLen = str.length();
        if(strLen < len)	{
            String rtnStr = str;
            for(int i= 0; i< len-strLen ; i++){
                rtnStr = rtnStr + chr;
            }
            return rtnStr;
        } else {
            return str;
        }
    }

    /**
     * 숫자 뒤로 특정문자열 채우기
     * @param n 숫자
     * @param len 숫자 최대길이
     * @param chr 숫자 뒤로 채울 특정문자
     * @return 특정문자열로 채워진 숫자 문자열
     */
    public static String pubCharR(int n, int len, String chr)	{
        return pubCharL(String.valueOf(n), len, chr);
    }

    /**
     * 전달받은 문자열 전체 byte size 반환
     * <br/><br/>
     * - 한글은 크기를 2로 영어는 1 그외 +- 기호는 1로 계산하여 길이값 되돌려 줍니다.<br/>
     * - 스마트 키패드에서 한국인은 한문 입력할 일이 없기때문에 국내에 맞게 한문은 고려하지 않습니다.<br/>
     *
     * @param str 문자열
     * @return 전달받은 문자열 전체 byte size
     */
    public static final int getByteSizeToComplex(String str) {
        int en = 0;
        int ko = 0;
        int etc = 0;

        char[] string = str.toCharArray();

        for (int j=0; j<string.length; j++) {
            if (string[j]>='A' && string[j]<='z') {
                en++;
            } else if (string[j]>='\uAC00' && string[j]<='\uD7A3') {
                ko++;
                ko++;
            } else {
                etc++;
            }
        }
        return (en + ko + etc);
    }

    /**
     * 전달받은 문자열에서 숫자만 반환
     *
     * @param value 문자열
     * @return 숫자 문자열
     */
    public static String getNumber(String value) {
        final String regEx = "[0-9]";
        if (value.matches(regEx)) return value;

        StringBuffer sb = new StringBuffer();
        for(int idx = 0; idx < value.length(); idx++) {
            if(value.substring(idx, idx+1).matches(regEx))
                sb.append(value.charAt(idx));
        }
        return sb.toString();
    }

    /**
     * 전달받은 문자열에서 숫자와 문자만 반환
     * <br/><br/>
     * - 특수문자(- , * ), 공백을 제거한 숫자와 문자(한글포함)만 리턴<br/>
     *
     * @param value 문자열
     * @return 숫자와 문자 문자열
     */
    public static String getNoSign(String value) {
        final String regEx = "\\w";
        if (value.matches(regEx)) return value;

        StringBuffer sb = new StringBuffer();
        for(int idx = 0; idx < value.length(); idx++) {
            if(value.substring(idx, idx+1).matches(regEx))
                sb.append(value.charAt(idx));
        }
        return sb.toString();
    }

    /**
     * 16자리 난수 문자열 반환
     * @return 16자리 난수 문자열
     */
    public static String getRandomKey() {
        Random r = new Random();

        StringBuffer sb = new StringBuffer();
        for(int i=0; i<16;i++) {
            sb.append(r.nextInt(9) + 1);
        }

        return sb.toString();
    }

    // TODO :: kkb :: convertHangul () 와 동일한 기능임, 하나는 삭제 가능, Convert.strAmount 클래스로 이동하는 것이 기능별로 분류될 듯
    /**
     * 한글금액표시 문자열 반환
     * <br/><br/>
     * - ex) 100,000->십만 <br>
     * - 만 ~ 조 까지 지원 <br>
     * @param value 금액 문자열
     * @return 한글금액표시 문자열 (100,000->십만)
     */
    public static String formatKoreanMoney(CharSequence value){
        final String [] krNumeric = {"", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구"};
        final String [] krUnit_L = {"", "십", "백", "천"};			// Lower
        final String [] krUnit_U = {"", "만", "억", "조"};			// Upper
        String amount = "";				// 한글금액
        String thdAmount = "";			// 천단위 변환금액
        int idx = 0;					//
        int pos = 0;
        boolean showUnit_L = false;
        boolean showUnit_U = false;

        char c = 0;

        CharSequence _Value = value.toString().replaceAll(",", "");

        pos = _Value.length();
        for(int i = 0; i < _Value.length(); i++){
            c = _Value.charAt(i);
            idx = c - 0x30;

            int share = (int)Math.floor(pos / 4.0);

            if(pos >= 9) {
                if(idx > 0){
                    showUnit_U = true;
                    thdAmount += krNumeric[idx];
                    thdAmount += krUnit_L[(pos-1)%4];
                }

                if(showUnit_U && pos == 9)
                    thdAmount += krUnit_U[share%4];
            } else if(pos >= 5) {
                if(idx > 0){
                    showUnit_L = true;
                    thdAmount += krNumeric[idx];
                    thdAmount += krUnit_L[(pos-1)%4];
                }

                if(showUnit_L && pos == 5)
                    thdAmount += krUnit_U[share%4];
            } else {
                if(idx > 0){
                    thdAmount += krNumeric[idx];
                    thdAmount += krUnit_L[(pos-1)%4];
                }
            }
            amount = thdAmount;
            pos--;
        }

        return amount;
    }

    // TODO :: kkb :: formatKoreanMoney () 와 동일한 기능임, 하나는 삭제 가능, Convert.strAmount 클래스로 이동하는 것이 기능별로 분류될 듯
    /**
     * 한글금액표시 문자열 반환
     * <br/><br/>
     * - ex) 100,000->십만 <br>
     * - 만 ~ 경 까지 지원 <br>
     * @param money 금액 문자열
     * @return 한글금액표시 문자열
     */
    public static String convertHangul(CharSequence money){
        String[] han1 = {"","일","이","삼","사","오","육","칠","팔","구"};
        String[] han2 = {"","십","백","천"};
        String[] han3 = {"","만","억","조","경"};

        String _Value = money.toString().replaceAll(",", "");

        boolean usehan3 = false;
        StringBuffer result = new StringBuffer();
        int len = _Value.length();
        for(int i=len-1; i>=0; i--){
            result.append(han1[Integer.parseInt(_Value.substring(len-i-1, len-i))]);
            if(Integer.parseInt(_Value.substring(len-i-1, len-i)) > 0) {
                result.append(han2[i % 4]);
                usehan3 = false;
            }

            if(i%4 == 0) {
                if( !usehan3 ) {
                    result.append(han3[i / 4]);
                }
                usehan3 = true;
            }

//			DevLog.eLog("Convert", "["+i+"] : " +result.toString());
        }

        return result.toString();
    }

    /**
     * DIP(Density Independent Pixel) 단위를  Pixel 단위로 변환
     *
     * @param atv Activity
     * @param nDip : dip 값
     * @return Pixel 값
     */
    public static int getDipToPixel(Activity atv, int nDip) {
        float scale = atv.getResources().getDisplayMetrics().density;
        return (int)(nDip * scale + 0.5f);
    }

    /**
     * DIP(Density Independent Pixel) 단위를  Pixel 단위로 변환
     *
     * @param context Context
     * @param nDip : dip 값
     * @return Pixel 값
     */
    public static int getDipToPixel(Context context, int nDip) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int)(nDip * scale + 0.5f);
    }

    /**
     * 순수 ClassName 반환
     *
     * @param ClassName 클래스이름
     * @return 순수 클래스이름
     */
    public static String getClassToClassName(String ClassName) {
        return ClassName.replace("class", "").replace(" ", "");

    }

    /**
     * 지역 / 지도 관련 처리 유틸
     *
     * @author Webcash Smart
     * @since 2017. 7. 20.
     */
    public static class Map {

        /**
         * 대한민국 지역명 변환
         * <br><br>
         * - 전달받은 지역 문자열 중 광역시 / 특별시 / 도 관련 문자열을 치환하여 반환 <br>
         * - 광역시 / 특별시 문자열 제거 : XX특별시 , XX광역시 => XX <br>
         * - 제주도 문자열 변환 : 제주도 => 제주 <br>
         * - 강원도 문자열 변환 : 강원도 => 강원 <br>
         * - 경기도 문자열 변환 : 경기도 => 경기 <br>
         * - 경상남도 문자열 변환 : 경상남도 => 경남 <br>
         * - 경상북도 문자열 변환 : 경상북도 => 경북 <br>
         * - 전라남도 문자열 변환 : 전라남도 => 전남 <br>
         * - 전라북도 문자열 변환 : 전라북도 => 전북 <br>
         * - 충청남도 문자열 변환 : 충청남도 => 충남 <br>
         * - 충청북도 문자열 변환 : 충청북도 => 충북 <br>
         *
         * @param value 지역 문자열
         * @return 지역명
         */
        public static String getFormatLocation(String value){
            String srtAddr = null;
            srtAddr = value.replaceAll("특별시", "").replaceAll("광역시", "");

            if(value.contains("제주도")){
                srtAddr = value.replaceAll("제주도", "제주");
            }
            if(value.contains("강원도")){
                srtAddr = value.replaceAll("강원도", "강원");
            }
            if(value.contains("경기도")){
                srtAddr = value.replaceAll("경기도", "경기");
            }
            if(value.contains("경상남도")){
                srtAddr = value.replaceAll("경상남도", "경남");
            }
            if(value.contains("경상북도")){
                srtAddr = value.replaceAll("경상북도", "경북");
            }
            if(value.contains("전라남도")){
                srtAddr = value.replaceAll("전라남도", "전남");
            }
            if(value.contains("전라북도")){
                srtAddr = value.replaceAll("전라북도", "전북");
            }
            if(value.contains("충청남도")){
                srtAddr = value.replaceAll("충청남도", "충남");
            }
            if(value.contains("충청북도")){
                srtAddr = value.replaceAll("충청북도", "충북");
            }
            return srtAddr;
        }
    }

    /**
     * 전화번호 관련 포맷 변환 처리
     *
     * @author Webcash Smart
     * @since 2017. 7. 20.
     */
    public static class PhoneNum {

        /**
         * 구분자가 없는 전화번호를 구분자(-)가 있는 전화번호로 반환
         * <br><br>
         * - 9자리 이상 11자리 이하인 경우에만 사용가능 <br>
         * - 휴대폰번호 (휴대전화번호 '010, 011...) <br>
         * - 지역번호가 포함된 전화번호 ('02', '032' ....) <br>
         *
         * @param num  전화번호 문자열
         * @return 구분자 (-) 가 추가된 전화번호 문자열
         */
        public static String formatPhoneNum(String num) {
            String result = num;
            String temp = num.replaceAll(" ", "").replaceAll("-", "");
            int len = temp.length();

            if(len < 9) {
                result = num;
            } else if(len >=9 && len<=11) {
                String prefix = temp.substring(1,2);
                if(prefix.equals("1")) {	// 휴대전화번호 '010, 011...'
                    if(len == 10) { // 10자리(010-123-4567)
                        result = temp.substring(0, 3);
                        result += "-";
                        result += temp.substring(3, 6);
                        result += "-";
                        result += temp.substring(6, 10);
                    } else if(len == 11){	// 11자리(010-1234-5678)
                        result = temp.substring(0, 3);
                        result += "-";
                        result += temp.substring(3, 7);
                        result += "-";
                        result += temp.substring(7, 11);
                    }
                }else if(prefix.equals("2")){	// 지역번호 서울(02)
                    if(len == 9) {	// 9자리(02-345-5678)
                        result = temp.substring(0, 2);
                        result += "-";
                        result += temp.substring(2, 5);
                        result += "-";
                        result += temp.substring(5, 9);
                    } else if(len == 10) {	// 10자리(02-3456-7890)
                        result = temp.substring(0, 2);
                        result += "-";
                        result += temp.substring(2, 6);
                        result += "-";
                        result += temp.substring(6, 10);
                    }
                } else {	// 지방번호(031, 051...)
                    if(len == 10) {	// 10자리(031-123-4567)
                        result = temp.substring(0, 3);
                        result += "-";
                        result += temp.substring(3, 6);
                        result += "-";
                        result += temp.substring(6, 10);
                    } else if(len == 11) {	// 11자리(031-3456-7890)
                        result = temp.substring(0, 3);
                        result += "-";
                        result += temp.substring(3, 7);
                        result += "-";
                        result += temp.substring(7, 11);
                    }
                }
            } else {
                return result;
            }
            return result;
        }

        /**
         * 구분자(-)가 있는 전화번호를을 구분자가 없는 전화번호로 반환
         *
         * @param num 전화번호 문자열
         * @return 구분자(-)가 없는 전화번호 문자열
         */
        public static String  nonformatPhoneNum(String num) {
            String result = "";
            result = num.replaceAll("-", "");
            return result;
        }
    }
}