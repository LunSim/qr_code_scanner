package com.kosign.qrcode.Utils;

import android.util.Log;
import android.webkit.ConsoleMessage;

/**
 * DevLog
 * <br><br>
 * 공통으로 사용되는 개발용 로그 <br>
 *
 * @author Webcash Smart
 * @since 2017.07
 **/
public class DevLog {

    /** 개발 / 운영 여부 */
    public static boolean DEBUG = true;
    /** 기본 태그 String */
    private static final String DEFAULT_TAG = "DEV_LOG";

    /**
     * Debug 사용 유무
     * <br><br>
     * @param use DEBUG 유무 true : 로그 출력, false : 로그 미출력
     */
    public static void setDebug(boolean use) {
        DEBUG = use;
    }

    /**
     * devLog
     * <br><br>
     * 개발버전만 로그를 출력, TAG : DEV_LOG, Priority : VERBOSE <br>
     * @param value	: log value
     */
    public static void devLog(String value) {

        // 배포용일 경우 로그 출력 하지않음.
        if (!DEBUG) {
            return;
        }

        largeLog( DEFAULT_TAG, value);
    }

    /**
     * devLog
     * <br><br>
     * 개발버전만 로그 출력, Priority : VERBOSE <br>
     * @param tag : tag
     * @param value : log value
     */
    public static void devLog(String tag, String value) {

        // 배포용일 경우 로그 출력 하지않음.
        if (!DEBUG) {
            return;
        }

        largeLog( tag, value);
    }

    /**
     * eLog
     * <br><br>
     * - 개발버전만 로그 출력 ,TAG : class name, Priority : ERROR <br>
     * - Tag 는 전달한 클래스의 이름 출력 <br>
     * @param cls : class
     * @param value : log value
     */
    public static void eLog(Class cls, String value) {

        // 배포용일 경우 로그 출력 하지않음.
        if (!DEBUG) {
            return;
        }

        Log.e(cls.getSimpleName(), value);
    }

    /**
     * eLog
     * <br><br>
     * 개발버전만 로그 출력, Priority : ERROR <br>
     * @param tag : tag
     * @param value : log value
     */
    public static void eLog(String tag, String value) {

        // 배포용일 경우 로그 출력 하지않음.
        if (!DEBUG) {
            return;
        }

        Log.e(tag, value);
    }
    /**
     * e
     * <br><br>
     * 개발버전만 로그 출력, TAG : DEV_LOG, Priority : ERROR <br>
     * @param value : log value
     */
    public static void e(Exception value) {
        // 배포용일 경우 로그 출력 하지않음.
        if (!DEBUG) {
            return;
        }

        Log.e(DEFAULT_TAG, value.getMessage());
    }

    /**
     * wLog
     * <br><br>
     * 개발버전만 로그 출력, Priority : WARN <br>
     * @param tag : tag
     * @param value : log value
     */
    public static void wLog(String tag, String value) {

        // 배포용일 경우 로그 출력 하지않음.
        if (!DEBUG) {
            return;
        }

        Log.w(tag, value);
    }
    /**
     * dLog
     * <br><br>
     * 개발버전만 로그 출력, Priority : DEBUG <br>
     * @param tag : tag
     * @param value : log value
     */
    public static void dLog(String tag, String value) {

        // 배포용일 경우 로그 출력 하지않음.
        if (!DEBUG) {
            return;
        }
        Log.d(tag, value);
    }
    /**
     * iLog
     * <br><br>
     * 개발버전만 로그 출력, Priority : INFO <br>
     * @param tag : tag
     * @param value : log value
     */
    public static void iLog(String tag, String value) {
        // 배포용일 경우 로그 출력 하지않음.
        if (!DEBUG) {
            return;
        }
        Log.i(tag, value);
    }

//	/**
//	 * WebView 로그 출력
//	 * <br><br>
//	 * 개발버전만 로그 출력, ConsoleMessage Level 에 따른 로그 레벨 설정
//	 * @param consoleMessage
//	 */
//	public static void webConsoleLog (ConsoleMessage consoleMessage) {
//		// 배포용일 경우 로그 출력 하지않음.
//		if (!DEBUG) {
//			return;
//		}
//
//		TYPE_LOG_LEVEL logLevel;
//		ConsoleMessage.MessageLevel consoleMsgLevel = consoleMessage.messageLevel();
//
//		if (consoleMsgLevel == ConsoleMessage.MessageLevel.TIP) {
//			logLevel = TYPE_LOG_LEVEL.TYPE_LOG_INFO;
//		} else if (consoleMsgLevel == ConsoleMessage.MessageLevel.ERROR) {
//			logLevel = TYPE_LOG_LEVEL.TYPE_LOG_ERROR;
//		} else if (consoleMsgLevel == ConsoleMessage.MessageLevel.WARNING) {
//			logLevel = TYPE_LOG_LEVEL.TYPE_LOG_WARN;
//		} else {
//			// consoleMsgLevel == ConsoleMessage.MessageLevel.LOG || consoleMsgLevel == ConsoleMessage.MessageLevel.DEBUG
//			logLevel = TYPE_LOG_LEVEL.TYPE_LOG_DEBUG;
//		}
//
//		largeLog(logLevel,
//				"WEBVIEW CONSOLE LOG ( " + consoleMessage.sourceId() + ", line :: " + consoleMessage.lineNumber() + " )",
//				consoleMessage.message()
//		);
//	}

    /**
     * largeLog
     * <br><br>
     * log 프린트 / 3000자가 넘는 log도 프린트, Priority : VERBOSE <br>
     * @param tag	: TAG
     * @param value	: log value
     */
    public static void largeLog(String tag, String value) {
        // 3000자가 넘을 경우 3000자까지만 프린트하고 나머지 값은 재귀함수를 호출한다.
        if (value.length() > 3000) {
            Log.v(tag, value.substring(0, 3000));

            // 재귀함수
            largeLog(tag, value.substring(3000));

            // 3000자가 넘지 않을 경우 모두 프린트한다.
        } else {
            Log.v(tag, value);
        }
    }

    /**
     * largeLog
     * <br><br>
     * log 프린트 / 3000자가 넘는 log도 프린트 됨, Priority : VERBOSE <br>
     * @param tag	: TAG
     * @param act  : Activity String
     * @param value	: log value
     */
    public static void largeLog(String tag, String act, String value) {

        // 3000자가 넘을 경우 3000자까지만 프린트하고 나머지 값은 재귀함수를 호출한다.
        if (value.length() > 3000) {
            Log.v(tag, act.toString() + " : " + value.substring(0, 3000));

            // 재귀함수
            largeLog(tag, value.substring(3000));

            // 3000자가 넘지 않을 경우 모두 프린트한다.
        } else {
            Log.v(tag, act.toString() + " : " + value);
        }
    }
}