package com.kosign.qrcode.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Environment;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;


import com.kosign.qrcode.R;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * AlertDialog Util
 * <br><br>
 * AlertDialog 를 생성 및 표시하고 버튼 이벤트를 Listener 를 통해 전달한다. <br>
 *
 * @author Webcash Smart
 * @since 2017. 7. 19.
 **/
public class DlgAlert {

    /** 호출한 Parent Activity */
    private static Activity mParentAct = null;

    /** Dialog 를 구분하기 위한 Index 기본값 */
    private static final int DEFAULT_DIALOG_INDEX = -9;

    /** Dialog Button enum */
    public enum DIALOG_BTN {
        LEFT_BTN, CENTER_BTN, RIGHT_BTN
    }

    /** Dialog Button Click Listener */
    public interface OnClickDlgListener {

        /**
         * Dialog Button Click Callback
         * @param dialogIndex Dialog 를 구분하기 위한 Index
         * @param buttonType Click 한 버튼 Type
         */
        void onClickDlgButton(int dialogIndex, DIALOG_BTN buttonType);
    }


    /**
     * AlertDialog Builder 를 Build 버전에 따라 다르게 생성한다.
     * <br><br>
     * - Build.VERSION_CODES.M 기준 <br>
     *
     * @param context Context
     * @return AlertDialog.Builder
     */
    public static AlertDialog.Builder getDialogCreate (Context context) {

        AlertDialog.Builder ad;

        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            ad = new AlertDialog.Builder(context);
        } else {
            ad = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
        }
        return ad;
    }

    /**
     * 알림 메세지창을 표시한 후, 확인버튼 클릭 시 OnClickDlgListener 를 호출한다
     * <br><br>
     * - DIALOG_BTN : CENTER_BTN <br>
     * - 단일버튼 ("확인", default)
     *
     * @param context context
     * @param title Dialog 타이틀
     * @param msg 알림 메세지
     * @param cancelable Dialog Cancelable
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     */
    public static void showAlert(Context context, String title, String msg, boolean cancelable, final OnClickDlgListener listener){
        AlertDialog.Builder ad = getDialogCreate(context);
        ad.setCancelable(cancelable);
        ad.setTitle(title);
        ad.setMessage(msg);
        ad.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(listener != null) {
                    listener.onClickDlgButton(DEFAULT_DIALOG_INDEX, DIALOG_BTN.CENTER_BTN);
                }
            }
        });
        ad.show();
    }

    /**
     * 알림 메세지창을 표시한 후, 확인버튼 클릭 시 OnClickDlgListener 를 호출한다
     * <br><br>
     * - DIALOG_BTN : CENTER_BTN <br>
     * - Dialog Title : "알림" (default) <br>
     *
     * @param con Context
     * @param msg 알림 메시지
     * @param cancelable Dialog Cancelable
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     */
    public static void showAlert(Context con, String msg, boolean cancelable, final OnClickDlgListener listener) {
        showAlert(con, con.getString(R.string.lib_dlg_alert_title), msg, cancelable, listener);
    }

    /**
     * 알림 메세지창을 표시한다.
     * <br><br>
     * - DIALOG_BTN : CENTER_BTN <br>
     * - Dialog Title : "알림" (default) <br>
     * - 단일버튼 ("확인", default)
     *
     * @param con Context
     * @param msg 알림 메시지
     * @param cancelable Dialog Cancelable
     */
    public static void showAlert(Context con, String msg, boolean cancelable) {
        showAlert(con, con.getString(R.string.lib_dlg_alert_title), msg, cancelable, null);
    }

    /**
     * 알림 메세지창을 표시한 후, 확인버튼 클릭시 OnClickDlgListener 를 호출한다
     * <br><br>
     * - DIALOG_BTN : CENTER_BTN <br>
     * - Dialog Title : "알림" (default) <br>
     * - 단일버튼 ("확인", default)
     *
     * @param context context
     * @param msg 알림메세지
     * @param dialogIndex Dialog 를 구분하기 위한 index
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     * @param cancelable Dialog Cancelable
     */
    public static void showAlert(Context context, String msg, final int dialogIndex, final OnClickDlgListener listener, boolean cancelable ) {
        AlertDialog.Builder ad = getDialogCreate(context);

        ad.setTitle(R.string.lib_dlg_alert_title);
        ad.setMessage(msg);
        ad.setCancelable(cancelable);
        ad.setPositiveButton(context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(listener != null) {
                    listener.onClickDlgButton(dialogIndex, DIALOG_BTN.CENTER_BTN);
                }
            }
        });
        AlertDialog dialog = ad.show();

        TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
    }

    /**
     * 알림 메세지창을 표시한 후, 확인버튼 클릭시 OnClickDlgListener 를 호출한다
     * <br><br>
     * - DIALOG_BTN : CENTER_BTN <br>
     * - Dialog Title : "알림" (default) <br>
     * - 단일버튼 ("확인", default)
     *
     * @param con Context
     * @param msg 알림 메시지
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     */
    public static void showAlertOk(Context con, String msg, final OnClickDlgListener listener) {
        showAlert(con, msg, DEFAULT_DIALOG_INDEX, listener, true);
    }

    /**
     * 알림메세지창을 활성화 한후, left, right 버튼 클릭 시 OnClickDlgListener 를 호출한다.
     * <br><br>
     * - DIALOG_BTN : LEFT_BTN, RIGHT_BTN <br>
     * - 좌측 부정, 우측 긍정 <br>
     *
     * @param context Context
     * @param title Dialog 타이틀
     * @param msg 알림 메시지
     * @param dialogIndex Dialog 를 구분하기 위한 Index
     * @param leftName Dialog 좌측 버튼명
     * @param rightName Dialog 우측 버튼명
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     * @param cancelable Dialog Cancelable
     */
    public static void showAlertOkCancel(final Context context, String title, String msg, final int dialogIndex, String leftName, String rightName, final OnClickDlgListener listener, boolean cancelable){
        AlertDialog.Builder ad = getDialogCreate(context);

        ad.setTitle(title);
        ad.setMessage(msg);
        ad.setCancelable(cancelable);
        // 긍정/오른쪽
        ad.setPositiveButton(rightName, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(listener != null) {
                    listener.onClickDlgButton(dialogIndex, DIALOG_BTN.RIGHT_BTN);
                }
            }
        });
        // 부정/왼쪽
        ad.setNegativeButton(leftName, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(listener != null) {
                    listener.onClickDlgButton(dialogIndex, DIALOG_BTN.LEFT_BTN);
                }
            }
        });
        AlertDialog dialog = ad.show();

        TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
    }

    /**
     * 알림메세지창을 활성화 한후, left, right 버튼 클릭 시 OnClickDlgListener 를 호출한다.
     * <br><br>
     * - DIALOG_BTN : LEFT_BTN, RIGHT_BTN <br>
     * - Dialog Title : "알림" (default) <br>
     * - 좌측 부정 (취소), 우측 긍정 (확인) <br>
     * - cancelable : true <br>
     *
     * @param con Context
     * @param msg 알림 메시지
     * @param dialogIndex Dialog 를 구분하기 위한 Index
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     */
    public static void showAlertOkCancel(Context con, String msg, final int dialogIndex, final OnClickDlgListener listener){
        showAlertOkCancel(con, con.getString(R.string.lib_dlg_alert_title), msg, dialogIndex, con.getString(android.R.string.cancel), con.getString(android.R.string.ok),  listener, true);
    }

    /**
     * 알림메세지창을 활성화 한후, left, right 버튼 클릭 시 OnClickDlgListener 를 호출한다.
     * <br><br>
     * - DIALOG_BTN : LEFT_BTN, RIGHT_BTN <br>
     * - Dialog Title : "알림" (default) <br>
     * - 좌측 부정 (취소), 우측 긍정 (확인) <br>
     * - cancelable : true <br>
     *
     * @param con Context
     * @param msg 알림 메시지
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     */
    public static void showAlertOkCancel(Context con, String msg, final OnClickDlgListener listener){
        showAlertOkCancel(con, msg, DEFAULT_DIALOG_INDEX, listener);
    }

    /**
     * 알림메세지창을 활성화 한후, left, right 버튼 클릭 시 OnClickDlgListener 를 호출한다.
     * <br><br>
     * - DIALOG_BTN : LEFT_BTN, RIGHT_BTN <br>
     * - 좌측 부정 (취소), 우측 긍정 (확인) <br>
     * - cancelable : true <br>
     *
     * @param con Context
     * @param title Dialog 타이틀
     * @param msg 알림 메시지
     * @param dialogIndex Dialog 를 구분하기 위한 Index
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     */
    public static void showAlertOkCancel(Context con, String title, String msg, final int dialogIndex, final OnClickDlgListener listener){
        showAlertOkCancel(con, title, msg, dialogIndex, con.getString(android.R.string.ok), con.getString(android.R.string.cancel), listener, true);
    }

    /**
     * 알림메세지창을 활성화 한후, left, right 버튼 클릭 시 OnClickDlgListener 를 호출한다.
     * <br><br>
     * - DIALOG_BTN : LEFT_BTN, RIGHT_BTN <br>
     * - Dialog Title : "알림" (default) <br>
     * - 좌측 부정, 우측 긍정 <br>
     * - cancelable : true <br>
     *
     * @param con Context
     * @param msg 알림 메시지
     * @param leftName Dialog 좌측 버튼명
     * @param rightName Dialog 우측 버튼명
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     */
    public static void showAlertOkCancle(Context con, String msg, String leftName, String rightName, OnClickDlgListener listener ){
        showAlertOkCancel(con, con.getString(R.string.lib_dlg_alert_title), msg, DEFAULT_DIALOG_INDEX, leftName, rightName, listener, true);
    }

    /**
     * 알림메세지창을 활성화 한후, left, right 버튼 클릭 시 OnClickDlgListener 를 호출한다.
     * <br><br>
     * - DIALOG_BTN : LEFT_BTN, RIGHT_BTN <br>
     * - Dialog Title : "알림" (default) <br>
     * - 좌측 부정, 우측 긍정 <br>
     * - cancelable : true <br>
     *
     * @param con Context
     * @param msg 알림 메시지
     * @param dialogIndex Dialog 를 구분하기 위한 Index
     * @param leftName Dialog 좌측 버튼명
     * @param rightName Dialog 우측 버튼명
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     */
    public static void showAlertOkCancel(Context con, String msg, final int dialogIndex, String leftName, String rightName, final OnClickDlgListener listener){
        showAlertOkCancel(con, con.getString(R.string.lib_dlg_alert_title), msg, dialogIndex, leftName, rightName, listener, true);
    }

    // TODO :: kkb :: 퍼미션 공통에서 사용 중 .... 확인 후 리뷰 시 삭제 및 대체 여부 결정 필요
    /**
     * left cancel, right ok
     * @param con context
     * @param msg msg
     * @param listener listener
     */
    public static void showAlertCancelOk(Context con, String msg, final OnClickDlgListener listener){
        showAlertCancelOk(con, msg, DEFAULT_DIALOG_INDEX, listener);
    }

    // TODO :: kkb :: 퍼미션 공통에서 사용 중 .... 확인 후 리뷰 시 삭제 및 대체 여부 결정 필요
    public static void showAlertCancelOk(Context con, String msg, final int dialogIndex, final OnClickDlgListener listener){
        showAlertOkCancel(con, con.getString(R.string.lib_dlg_alert_title), msg, dialogIndex, con.getString(android.R.string.cancel), con.getString(android.R.string.ok), listener, true);
    }

    // TODO :: kkb :: 퍼미션 공통에서 사용 중 .... 확인 후 리뷰 시 삭제 및 대체 여부 결정 필요
    /**
     * 알림 메세지창을 표시한 후, left, center, right 버튼 클릭 시 OnClickDlgListener 를 호출한다.
     * <br><br>
     *
     * - DIALOG_BTN : LEFT_BTN, CENTER_BTN, RIGHT_BTN
     * - Dialog Title : "알림" (default) <br>
     * - 좌측 부정, 우측 긍정 <br>
     * - cancelable : true <br>
     *
     * @param context context
     * @param msg 알림 메세지
     * @param leftName Dialog 좌측 버튼명
     * @param centerName Dialog 가운데 버튼명
     * @param rightName Dialog 우측 버튼명
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     * @param cancelable Dialog Cancelable
     */
    public static void showAlertAllOneCancel(final Context context, String msg,  String leftName, String centerName, String rightName, final OnClickDlgListener listener, boolean cancelable){
        AlertDialog.Builder ad = getDialogCreate(context);

        ad.setTitle(R.string.lib_dlg_alert_title);
        ad.setMessage(msg);
        ad.setCancelable(cancelable);
        ad.setPositiveButton(rightName, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                listener.onClickDlgButton(0, DIALOG_BTN.RIGHT_BTN);
            }
        });
        ad.setNeutralButton(centerName, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                listener.onClickDlgButton(0, DIALOG_BTN.CENTER_BTN);
            }
        });
        ad.setNegativeButton(leftName, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                listener.onClickDlgButton(0, DIALOG_BTN.LEFT_BTN);
            }
        });
        ad.show();
    }

    /**
     * 알림 메세지창을 표시한 후, left, center, right 버튼 클릭 시 OnClickDlgListener 를 호출한다.
     * <br><br>
     *
     * - DIALOG_BTN : LEFT_BTN, CENTER_BTN, RIGHT_BTN
     * - Dialog Title : "알림" (default) <br>
     * - 좌측 부정, 우측 긍정 <br>
     *
     * @param con
     * @param msg
     * @param centerName
     * @param listener
     * @param cancelable
     */
    public static void showAlertAllOneCancel(Context con, String msg, String centerName, final OnClickDlgListener listener, boolean cancelable){
        showAlertAllOneCancel(con, msg, con.getString(android.R.string.ok), centerName, con.getString(android.R.string.cancel), listener, cancelable);
    }

    /**
     * 알림 메세지창을 표시한 후, 확인버튼 클릭 시 해당 액티비티를 종료한다.
     * <br><br>
     *
     * - Dialog Title : "알림" (default) <br>
     * - 단일버튼 ("확인", default)
     * - cancelable : true <br>
     *
     * @param act 액티비티
     * @param msg 알림 메세지
     */
    public static void showAlertAfterFinish(Activity act, String msg) {
        mParentAct = act;

        AlertDialog.Builder ad = getDialogCreate(act);

        ad.setTitle(R.string.lib_dlg_alert_title);
        ad.setMessage(msg);
        ad.setCancelable(true);
        ad.setPositiveButton(act.getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        mParentAct.finish();
                    }
                });
        ad.show();
    }

    /**
     * 알림 메세지창을 표시한 후, 확인버튼 클릭 시 해당 액티비티를 종료한다.
     * <br><br>
     *
     * - Dialog Title : "알림" (default) <br>
     * - 좌측 부정, 우측 긍정 <br>
     * - cancelable : true <br>
     *
     * @param act 액티비티
     * @param msg 알림 메세지
     * @param listener Dialog Button Click Listener (OnClickDlgListener)
     */
    public static void showConfirmAfterFinish(Activity act, String msg, final OnClickDlgListener listener) {
        mParentAct = act;

        AlertDialog.Builder ad = getDialogCreate(act);

        ad.setTitle(R.string.lib_dlg_alert_title);
        ad.setMessage(msg);
        ad.setCancelable(true);
        // 긍정/오른쪽
        ad.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(listener != null) {
                    listener.onClickDlgButton(DEFAULT_DIALOG_INDEX, DIALOG_BTN.RIGHT_BTN);
                }
            }
        });
        // 부정/왼쪽
        ad.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(listener != null) {
                    listener.onClickDlgButton(DEFAULT_DIALOG_INDEX, DIALOG_BTN.LEFT_BTN);
                }
            }
        });
        ad.show();
    }

    /** Error 발생 시 로그 파일 저장 여부
     * <br><br>
     * - default false (저장하지 않음) <br>
     **/
    private static boolean mErrorLogSave = false;

    // TODO :: kkb :: 정리 및 삭제 필요, 제대로 사용되지 않고 있음...
    /** Error 발생 시 Error 메시지창의 알림 메시지 치환 구분자 */
    private static final String regularExpr = "&";

    /**
     * Error 발생 시 로그 파일 저장 여부 설정
     *
     * @param value true (저장) / false (저장하지 않음)
     */
    public static void setErrprLogSave(boolean value) {
        mErrorLogSave = value;
    }

    /**
     * Error 발생 시 로그 파일 저장 여부 반환
     *
     * @return true (저장) / false (저장하지 않음)
     */
    public static boolean isLogSave() {
        return mErrorLogSave;
    }

    // TODO :: kkb :: 정리 및 삭제 필요, 제대로 사용되지 않고 있음...
    /**
     * Error 메세지창을 표시한다.
     *
     * @param context context
     * @param msg 알림 메시지 <br>
     *
     * @param vargs
     */
    public static void showError(Context context, String msg, String... vargs) {
        for (String var : vargs) {
            msg = msg.replaceFirst(regularExpr, var);
        }

        AlertDialog.Builder ad = getDialogCreate(context);

        ad.setTitle(R.string.lib_dlg_error_title).setMessage(msg)
                .setNeutralButton(context.getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                            }
                        }).show();
    }

    // TODO :: kkb :: 정리 및 삭제 필요, 제대로 사용되지 않고 있음..., 파일 저장 시 퍼미션 체크도 필요함...
    /**
     *
     * @param context
     * @param msg
     * @param e
     */
    public static void showError(Context context, String msg, Exception e) {
//		PrintLog.printException(act, msg, e);
        e.printStackTrace();

        if (mErrorLogSave) {
            String errMsg = e.getLocalizedMessage() + "\n\r";
            errMsg += e.getMessage();
            for(int i = 0; i < e.getStackTrace().length; i++) {
                errMsg += e.getStackTrace()[i] + "\n\r";
            }

            String path = Environment.getExternalStorageDirectory() + "/WEBCASH";
            File logFile = new File(path);
            if (!logFile.exists()) {
                logFile.mkdir();
            }

            String fileName = "/" +Convert.ComDate.today() + "_error.log";
            logFile = new File(path + fileName);
            if (!logFile.exists()) {
                try {
                    logFile.createNewFile();
                } catch (IOException exp) {
                    exp.printStackTrace();
                }
            }

            try {
                //BufferedWriter for performance, true to set append to file flag
                BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
                buf.append("\n\r\n\r" + Convert.ComDate.currDateTimeMillisend() + "\n\r");
                buf.append(errMsg);
                buf.newLine();
                buf.close();
            } catch (IOException exp) {
                exp.printStackTrace();
            }

        }

        String var = e.getStackTrace()[0].toString();
        showError(context, msg, var);
    }

    // TODO :: kkb :: 정리 및 삭제 필요, 제대로 사용되지 않고 있음...
    /**
     *
     * @param context
     * @param e
     */
    public static void showError(Context context, Exception e) {
//		PrintLog.printException(act, e.getMessage(), e);

		/*
		cf) e.getStackTrace()[0].toString(); -> at com.webcash.smart.sample_smart_lib.MainActivity$1.onClick(MainActivity.java:31)
		cf) e.getMessage -> Fake null pointer exception

07-20 10:30:41.432 1730-1730/com.webcash.smart.sample_smart_lib D/ViewRootImpl@78447c9[MainActivity]: setView = DecorView@fa7fece[MainActivity] touchMode=true
07-20 10:30:41.432 1730-1730/com.webcash.smart.sample_smart_lib W/System.err: java.lang.NullPointerException: Fake null pointer exception
07-20 10:30:41.432 1730-1730/com.webcash.smart.sample_smart_lib W/System.err:     at com.webcash.smart.sample_smart_lib.MainActivity$1.onClick(MainActivity.java:31)
07-20 10:30:41.432 1730-1730/com.webcash.smart.sample_smart_lib W/System.err:     at android.view.View.performClick(View.java:6205)
07-20 10:30:41.433 1730-1730/com.webcash.smart.sample_smart_lib W/System.err:     at android.widget.TextView.performClick(TextView.java:11103)
07-20 10:30:41.433 1730-1730/com.webcash.smart.sample_smart_lib W/System.err:     at android.view.View$PerformClick.run(View.java:23653)
07-20 10:30:41.433 1730-1730/com.webcash.smart.sample_smart_lib W/System.err:     at android.os.Handler.handleCallback(Handler.java:751)
07-20 10:30:41.433 1730-1730/com.webcash.smart.sample_smart_lib W/System.err:     at android.os.Handler.dispatchMessage(Handler.java:95)
07-20 10:30:41.433 1730-1730/com.webcash.smart.sample_smart_lib W/System.err:     at android.os.Looper.loop(Looper.java:154)
07-20 10:30:41.433 1730-1730/com.webcash.smart.sample_smart_lib W/System.err:     at android.app.ActivityThread.main(ActivityThread.java:6682)
07-20 10:30:41.433 1730-1730/com.webcash.smart.sample_smart_lib W/System.err:     at java.lang.reflect.Method.invoke(Native Method)
07-20 10:30:41.433 1730-1730/com.webcash.smart.sample_smart_lib W/System.err:     at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:1520)
07-20 10:30:41.433 1730-1730/com.webcash.smart.sample_smart_lib W/System.err:     at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:1410)
		 */
        String var = e.getStackTrace()[0].toString();
        showError(context, e.getMessage(), var);
    }
}