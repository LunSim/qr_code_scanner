package com.kosign.qrcode.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.Html;
import android.widget.TextView;
import com.kosign.qrcode.R;

/**
 * UI 유틸리티
 * @author Webcash Smart
 * @since 2019-09-24 13:40
 **/
public class UIUtil {

    /**
     * Alert 형태의 팝업 표시
     * <br><br>
     * - title : 알림 <br>
     * - cancelable : false <br>
     * - 버튼 : 확인 <br>
     * - 버튼 ClickListener : null <br>
     * @param context Context
     * @param msg 메시지
     * @return AlertDialog
     */
    public static AlertDialog showAlertPopup (Context context, String msg) {
        return showPopup(context, context.getString(R.string.title_popup_dlg), msg, null, context.getString(R.string.btn_confirm));
    }

    /**
     * Alert 형태의 팝업 표시
     * <br><br>
     * - title : 알림 <br>
     * - cancelable : false <br>
     * - 버튼 : 확인 <br>
     * @param context Context
     * @param msg 메시지
     * @param btnClickListener 버튼 ClickListener
     *                         <br><br>
     *                         - Click Button : DialogInterface.BUTTON_POSITIVE <br>
     * @return AlertDialog
     */
    public static AlertDialog showAlertPopup (Context context, String msg, DialogInterface.OnClickListener btnClickListener) {
        return showPopup(context, context.getString(R.string.title_popup_dlg), msg, btnClickListener, context.getString(R.string.btn_confirm));
    }

    /**
     * Confirm 형태의 팝업 표시
     * <br><br>
     * - title : 알림 <br>
     * - cancelable : false <br>
     * - 버튼 : 확인 / 취소 <br>
     * @param context Context
     * @param msg 메시지
     * @param btnClickListener 버튼 ClickListener
     *                         <br><br>
     *                         - 확인 Click Button ( buttons[0] ) : DialogInterface.BUTTON_POSITIVE <br>
     *                         - 취소 Click Button ( buttons[1] ) : DialogInterface.BUTTON_NEGATIVE <br>
     * @return AlertDialog
     */
    public static AlertDialog showConfirmPopup (Context context, String msg, DialogInterface.OnClickListener btnClickListener) {
        return showPopup(context, context.getString(R.string.title_popup_dlg), msg, btnClickListener,
                context.getString(R.string.btn_confirm), context.getString(R.string.btn_cancel)
        );
    }

    /**
     * Dialog 표시
     * <br><br>
     * - cancelable : false <br>
     * @param context Context
     * @param title 타이틀
     * @param msg 메시지
     * @param btnClickListener 버튼 ClickListener
     *                         <br><br>
     *                         - buttons[0] Click : DialogInterface.BUTTON_POSITIVE <br>
     *                         - buttons[1] Click : DialogInterface.BUTTON_NEGATIVE <br>
     *                         - buttons[2] Click : DialogInterface.BUTTON_NEUTRAL <br>
     * @param buttons 버튼 배열 (최대 3게)
     *                <br><br>
     *                - buttons[0] : PositiveButton <br>
     *                - buttons[1] : NegativeButton <br>
     *                - buttons[2] : NeutralButton <br>
     * @return AlertDialog
     */
    public static AlertDialog showPopup (Context context, String title, String msg,
                                         DialogInterface.OnClickListener btnClickListener, String... buttons) {
        return showPopup(context, title, msg, null, btnClickListener, buttons);
    }

    /**
     * Dialog 표시
     * @param context Context
     * @param title 타이틀
     * @param msg 메시지
     * @param cancelListener 취소 Listener
     *                       <br><br>
     *                       - null 이면 cancelable : false <br>
     *                       - null 이 아니면 cancelable : true <br>
     * @param btnClickListener 버튼 ClickListener
     *                         <br><br>
     *                         - buttons[0] Click : DialogInterface.BUTTON_POSITIVE <br>
     *                         - buttons[1] Click : DialogInterface.BUTTON_NEGATIVE <br>
     *                         - buttons[2] Click : DialogInterface.BUTTON_NEUTRAL <br>
     * @param buttons 버튼 배열 (최대 3게)
     *                <br><br>
     *                - buttons[0] : PositiveButton <br>
     *                - buttons[1] : NegativeButton <br>
     *                - buttons[2] : NeutralButton <br>
     * @return AlertDialog
     */
    public static AlertDialog showPopup (Context context, String title, String msg,
                                         DialogInterface.OnCancelListener cancelListener,
                                         DialogInterface.OnClickListener btnClickListener, String... buttons) {
        AlertDialog.Builder dlgBuilder = DlgAlert.getDialogCreate(context);
        dlgBuilder.setTitle(title);
        dlgBuilder.setMessage(msg);

        if (cancelListener == null) {
            dlgBuilder.setCancelable(false);
        } else {
            dlgBuilder.setCancelable(true);
            dlgBuilder.setOnCancelListener(cancelListener);
        }

        if (buttons.length >= 1) {
            dlgBuilder.setPositiveButton(buttons[0], btnClickListener);
        }

        if (buttons.length >= 2) {
            dlgBuilder.setNegativeButton(buttons[1], btnClickListener);
        }

        if (buttons.length >= 3) {
            dlgBuilder.setNegativeButton(buttons[2], btnClickListener);
        }

        return dlgBuilder.show();
    }

    /**
     * TextView HTML 적용
     * @param textView TextView
     * @param text HTML Text
     */
    public static void setHTMLText (TextView textView, String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(
                    Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY));
        } else {
            textView.setText(
                    Html.fromHtml(text));
        }
    }
}
