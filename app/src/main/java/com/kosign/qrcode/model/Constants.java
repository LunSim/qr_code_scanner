package com.kosign.qrcode.model;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

public class Constants {

    public static final String T_MONEY_CARD = "90000001";

    public static final String COUNT               = "COUNT";

    public static class LoginInfo {
        public static final String USER_ID           = "USER_ID";
        public static final String PASSWORD           = "PASSWORD";
        public static final String NATION_CD        = "NATION_CD";
        public static final String INTENT_NATION_CD        = "nation";
    }

    public static class ReceiptInfo {
        public static boolean WILL_RELOAD                   = true;
        public static boolean RELOAD_RECEIPT                = false;
        public static boolean SHOW_SEND_SUCCESS_SNACKBAR    = false;  // send success
        public static boolean SHOW_UPDATE_SNACKBAR          = false;  // update success
        public static boolean SHOW_MOVE_SUCCESS_SNACKBAR    = false;  // move success
        public static boolean SHOW_CANCEL_SUCESS_SNACKBAR   = false;  // cancel success
        public static boolean NEW_RECEIPT_SUBMIT            = false;
        public static boolean SHOW_PAYMENT_REQUEST_CANCELED = false;  // payment request canceled
        public static boolean SHOW_GROUP_SEND_SNACKBAR      = false;
    }

    public static class MycardInfo {
        public static String RELOAD_CARD_RESUME = "RELOAD_CARD_RESUME";
        public static String PREVIOUS_USER      = "DEMOZ";
        public static String NEW_CARD           = "";
        public static String NEW_CARD_CORP_CD   = "";
        public static boolean ADD_NEW_CARD      = false;
        public static boolean RELOAD_CARD       = false;
    }

    public static class MainInfo {
        public static String selected_card_number       = "";
        public static String selected_card_name         = "";
        public static String selected_card_corp_cd      = "";
        public static String selected_card_org_name     = "";
        public static String selected_card_reg_sts      = "";
        public static String selected_card_module       = "";
        public static String selected_card_start_date   = "";
        public static String selected_card_bynm         = "";
        public static String selected_card_manager_name = "Manager Name";
        public static String APP_SELECTED_CARD_NUMBER   = "";
        public static boolean is_first_load_card        = true;
        public static boolean LOAD_PUSH                 = false;   // for Push while app is running
        public static boolean PUSH_GCM                  = false;   // for check if app is run from Push or normal click
        public static String selected_card_oversea_date = "";
    }

    public static class ReceiptDetail{
       // public static List<AddnItem> TEMP_LIST = new ArrayList<>();
       /// public static List<AddnItem> TEMP_LIST_SELECTED = new ArrayList<>();
        public static final String TITLE        = "TITLE";
        public static final String CARD_CORP_CD = "CARD_CORP_CD";
        public static final String CARD_NO      = "CARD_NO";
        public static final String API_ITEM_GB  = "API_ITEM_GB";
        public static final String TEXT_TOP     = "TEXT_TOP";
        public static final String ITEM_CD      = "ITEM_CD";
        // Update 21.11.18
        public static final String TXNO         = "TXNO";
        public static final String SAVE_DATA    = "PRE_DATA";
        public static final String CD           = "CD";
        public static final String NM           = "NM";
        public static Bitmap TAKE_PHOTO_BITMAP;
        public static String TAKE_PHOTO_FILE_NAME;
        public static String HINT               = "HINT";

        public static final String ENTR_SEL_GB      = "ENTR_SEL_GB";
    }

    public static class ScrapingInfo{
        public static final String CLASS = "개인카드";
    }
    public static class NotificationSetting{
        public static final String IS_RECOMMAND_ON="IS_RECOMMAND_ON";
    }

    public static class ExternalPayment{
        public static final String CARD_CORP_CD = "EX_CARD_CORP_CD";
        public static final String CARD_NO = "EX_CARD_NO";
        public static final String APV_DT = "EX_APV_DT";
    }

    public static class PickPhoto {
        public static final int PHOTOPICK = 100;
        public static Bitmap TAKE_PHOTO_BITMAP;
        public static String TAKE_PHOTO_FILE_NAME;
        public static String TAKE_PHOTO_FILE_PATH;
    }

    public static class MG{
        public static final String C_DAOU_ERP_URL   = "C_DAOU_ERP_URL";
        public static final String C_HUB_LINK_URL   = "C_HUB_LINK_URL";
        public static final String C_POPUP_YN   = "C_POPUP_YN";
        public static final String C_ASP_YN   = "C_ASP_YN";
    }
}
