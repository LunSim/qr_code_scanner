package com.kosign.qrcode.model;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kosign.qrcode.R;
import com.kosign.qrcode.model.DlgWindow;

public class LanguageDlgAlert {
    private static OnDialogListener mlistener;
    private static DlgWindow dialogWindow;

    public enum DIALOG_BTN {
        LEFT_BTN, RIGHT_BTN
    }

    public static void DlgAlertOk(Context context, String alert_title, String alert_message, String alert_right, OnDialogListener listener) {
        mlistener = listener;

        View mLayout = View.inflate(context, R.layout.comm_dialog_language, null);
        dialogWindow = new DlgWindow(context, mLayout);

        TextView tv_title    = (TextView) mLayout.findViewById(R.id.tv_dlg_title);
        TextView tv_message  = (TextView) mLayout.findViewById(R.id.tv_dlg_message);

        Button mOk     = (Button) mLayout.findViewById(R.id.btn_dlg_ok);

        tv_title.setText(alert_title);
        tv_message.setText(alert_message);

        mLayout.findViewById(R.id.ll_cancel_body).setVisibility(View.GONE);

        mOk.setText(alert_right);
        mOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogWindow.dismiss();
                if (mlistener != null)
                    mlistener.onClickDlgButton(1, DIALOG_BTN.RIGHT_BTN);
            }
        });
    }

    public static void DlgAlertOkCancel(Context context,  String alert_title, String alert_message, String alert_left, String alert_right, OnDialogListener listener) {
        mlistener = listener;

        View mLayout = View.inflate(context, R.layout.comm_dialog_language, null);
        dialogWindow = new DlgWindow(context, mLayout);

        TextView tv_title    = (TextView) mLayout.findViewById(R.id.tv_dlg_title);
        TextView tv_message  = (TextView) mLayout.findViewById(R.id.tv_dlg_message);

        Button mOk     = (Button) mLayout.findViewById(R.id.btn_dlg_ok);
        Button mCancel = (Button) mLayout.findViewById(R.id.btn_dlg_cancel);

        tv_title.setText(alert_title);
        tv_message.setText(alert_message);

        mCancel.setText(alert_left);
        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogWindow.dismiss();
                if (mlistener != null)
                    mlistener.onClickDlgButton(2, DIALOG_BTN.LEFT_BTN);
            }
        });

        mOk.setText(alert_right);
        mOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogWindow.dismiss();
                if (mlistener != null)
                    mlistener.onClickDlgButton(1, DIALOG_BTN.RIGHT_BTN);
            }
        });
    }

    public interface OnDialogListener {
        void onClickDlgButton(int dialogIndex, DIALOG_BTN buttonType);
    }
}
