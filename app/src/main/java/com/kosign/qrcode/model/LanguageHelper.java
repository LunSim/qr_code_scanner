package com.kosign.qrcode.model;

import android.content.Context;
import android.util.Log;

import com.kosign.qrcode.R;

import java.util.ArrayList;

public class LanguageHelper {
    public static ArrayList<LanguageItemObject> getLanguageItems(Context context) {
       ArrayList<LanguageItemObject> languageItemObjects = new ArrayList<>();

       LanguageItemObject lang = new LanguageItemObject();
       lang.setLang_id(1);
       lang.setLocal_key(context.getResources().getString(R.string.LANGUAGE_FLAG_KO));
       lang.setIso_code("ko");
       lang.setNation_cd("");
       lang.setFlag_code("flg_kr");
       languageItemObjects.add(lang);

        lang = new LanguageItemObject();
        lang.setLang_id(2);
        lang.setLocal_key(context.getResources().getString(R.string.LANGUAGE_FLAG_EN));
        lang.setIso_code("en");
        lang.setNation_cd("840");
        lang.setFlag_code("flg_us");
        languageItemObjects.add(lang);

        lang = new LanguageItemObject();
        lang.setLang_id(3);
        lang.setLocal_key(context.getResources().getString(R.string.LANGUAGE_FLAG_ZH));
        lang.setIso_code("zh");
        lang.setNation_cd("156");
        lang.setFlag_code("flg_cn");
        languageItemObjects.add(lang);

        lang = new LanguageItemObject();
        lang.setLang_id(4);
        lang.setLocal_key(context.getResources().getString(R.string.LANGUAGE_FLAG_JP));
        lang.setIso_code("ja");
        lang.setNation_cd("392");
        lang.setFlag_code("flg_jp");
        languageItemObjects.add(lang);

        return languageItemObjects;
    }
    /*
     * convert string name drawable to drawable id
     */
    public static int getImageFromDrawable(Context context, String imageName){
        return context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());
    }

    /*
     * convert string lowercase to uppercase
     */
    public static String initConvertLanguageUpperCase(String iso) {
        return (iso.equals("ko") ? "df" : iso).toUpperCase();
    }
}
