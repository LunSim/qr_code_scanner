package com.kosign.qrcode.model;

public class LanguageItemObject {
    private int lang_id;
    private String local_key;
    private String iso_code;
    private String flag_code;
    private String nation_cd;

    /**
     * use for get lang_id
     * */
    public int getLang_id(){
        return lang_id;
    }

    /**
     * set lang_id
     * */
    public void setLang_id(int lang_id){
        this.lang_id = lang_id;
    }

    public String getLocal_key(){
        return local_key;
    }
    public void setLocal_key(String local_key){
        this.local_key = local_key;
    }
    public String getIso_code(){
        return iso_code;
    }
    public void setIso_code(String ioIso_code){
        this.iso_code = ioIso_code;
    }
    public String getFlag_code(){
        return flag_code;
    }
    public void setFlag_code(String flag_code){
        this.flag_code = flag_code;
    }
    public String getNation_cd(){
        return nation_cd;
    }
    public void setNation_cd(String nation_cd){
        this.nation_cd = nation_cd;
    }

}
