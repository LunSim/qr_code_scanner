package com.kosign.qrcode.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * PreferenceDelegator
 * <br><br>
 * - 암호화 하여 SharedPreference 에 데이터 저장 <br>
 * - AES 암호화 방식 사용 <br>
 * - App 가 종료되더라도 저장되어야하는 데이터 관리
 *
 * @author Webcash Smart
 * @since 2017. 7. 20.
 **/
public class PreferenceDelegator {

    /** PreferenceDelegator Instance */
    private static PreferenceDelegator mInstance;

    /** PreferenceDelegator 에서 데이터를 저장하고 있는 SharedPreferences */
    private SharedPreferences mPreferences;

    /**
     * 생성자
     * @param context Context
     */
    private PreferenceDelegator(Context context) {
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * PreferenceDelegator Single Instance 생성, 반환 <br>
     * @param context Context
     * @return PreferenceDelegator Single Instance
     */
    public static PreferenceDelegator getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PreferenceDelegator(context);
        }
        return mInstance;
    }

    /**
     * Key 존재 여부 리턴 <br>
     * @param key Key
     * @return Key 존재 여부
     */
    public boolean contains(String key) {
        return this.mPreferences.contains(key);
    }

    // TODO :: kkb :: AESUtil 사용방식으로 변경 검토 필요
    /**
     * Preference KEY 로 저장된 String Value 복호화 하여 리턴
     * <br><br>
     * - AES 암호화 방식 사용 <br>
     * @param key Data Key
     * @return Key 에 해당하는 복호화된 데이터
     */
    public String get(String key) {
        String returnValue = "";
        try
        {
            if (this.mPreferences.contains(key)) {
                returnValue = this.mPreferences.getString(key, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnValue;
    }

    // TODO :: kkb :: AESUtil 사용방식으로 변경 검토 필요
    /**
     * Preference 에 해당 KEY-VALUE 로 String Value 암호화 하여 저장
     * <br><br>
     * - AES 암호화 방식 사용 <br>
     * @param key Data Key
     * @param value 저장할 String Value
     */
    public void put(String key, String value) {
        try {
            //SecretKeySpec sKeySpec = new SecretKeySpec(this.getEncKey().getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            //cipher.init(1, sKeySpec);
            //value = asHex(cipher.doFinal(value.getBytes()));

            this.mPreferences.edit().putString(key, value).commit();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Preference 해당 KEY-Value 삭제 <br>
     * @param key 삭제하고자 하는 Key
     */
    public void remove(String key) {
        this.mPreferences.edit().remove(key).commit();
    }

    /**
     * Preference Data 전체 삭제 <br>
     */
    public void removeAll() {
        this.mPreferences.edit().clear().commit();
    }

    // TODO :: kkb :: HexUtil 사용으로 변경 가능한지 검토 필요
    /**
     * Hex String > Byte 배열로 변환 <br>
     * @param s Byte 배열로 변환할 String
     * @return 변환된 Byte 배열
     */
    private byte[] asByte(String s) {
        int len = s.length();
        byte[] data = new byte[len % 2 == 0 ? len / 2 : len / 2 + 1];

        for (int i = 0; i < len; i += 2) {
            if (i + 1 < len)
                data[(i / 2)] = ((byte)((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16)));
            else {
                data[(i / 2)] = ((byte)(Character.digit(s.charAt(i), 16) << 4));
            }
        }
        return data;
    }

    // TODO :: kkb :: HexUtil 사용으로 변경 가능한지 검토 필요
    /**
     * Byte 배열 > Hex String 으로 변환 <br>
     * @param buf Hex String 으로 변환할 Byte 배열
     * @return 변환된 Hex String
     */
    private String asHex(byte[] buf) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);

        for (int i = 0; i < buf.length; i++) {
            if ((buf[i] & 0xFF) < 16) {
                strbuf.append("0");
            }
            strbuf.append(Long.toString(buf[i] & 0xFF, 16));
        }

        return strbuf.toString();
    }
}